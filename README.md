#Performance repositories mining

This is an implementation of a performance testing repository built using the Python programming language and the Django framework in the scope of my Master's thesis project. The performance testing repository is a web application aimed to store the results of load tests run to estimate performance of the target software. Apart from merely keeping these results in a centralized storage, a provided implementation of the repository also visualizes them and prepares the data for analysis using consolidation techniques. The performance testing repository supports several different classification algorithms and supplies a performance engineer with basic analysis tools that help to immediately detect severe performance degradations.

In addition to the performance testing repository, this project includes the analysis module that processes the results of the performance tests and reports the differences found between the runs. To find these differences, association rule mining techniques are used. 

The repository and the analysis module including a novel data mining algorithm called Dasha are described in [my Master's thesis document](http://repository.tudelft.nl/view/ir/uuid%3A076ec1f6-d6c6-4266-afc5-dd71e24d65b2/) in more details. 

## Installation at a development server

The code was tested with Python 2.7 run under Mac OS X. It is not guaranteed that it will work with the other versions of the language or under Windows. Frankly speaking, it is not 100% guaranteed that the application is going to launch with Python 2.7 and Mac OS X / Linux either, but that's another story.

MySQL 5.6.4+ is required, as earlier versions do not support working with fractional seconds precision, which is absolutely necessary.

To install the application, do the following.

* Clone the repository: `git clone git@bitbucket.org:dzzh/mining.git`
* Enter the repository: `cd mining`
* Optionally, install and activate a virtual environment. Refer to [virtualenv documentation](http://virtualenv.org) for the instructions.
* Install the dependencies using [pip](http://pip-installer.org): `pip install -r requirements.txt`. Pip is contained in a virtual environment installation, so you do not need to install it separately. 
* Install [matplotlib](matplotlib.org/users/installing.html). Installing it via pip does not always work as expected, hence it is suggested to install it manually following the instructions at the website. You may though try to install it with `pip install matplotlib` first, just in case. Let the force be with you.
* Patch python-mysql. See detailed instructions below.
* Start MySQL server and create an empty database. I suggest to call it `mining`, but you're not expected to obey silently. If you're not that familiar with MySQL, see more detailed instructions below. 
* Update database connection settings at `DATABASES` variable in `mining/mining/settings.py` file, if necessary. 
* Synchronize the database: `python manage.py syncdb`. At this step you will be asked to register a superuser. Don't worry, NSA won't get full access to these data. Just create a user as requested and use the provided credentials later on to log in.
* Start the embedded web server: `python manage.py runserver`.
* Verify that the server has correctly started by going to `http://127.0.0.1:8000` in your browser.

For sure, you do not need to go through all these steps each time you want to run the application. Once you have it installed, it is only needed to activate the virtual environment and run a server to start working.

### Patching python-mysql

Because of [a bug](http://sourceforge.net/p/mysql-python/bugs/325/) in python-mysql library, it cannot correctly process time entries with fractional seconds precision. A patch to fix this issue has to be applied. It can be done the following way.

* Copy `patch/times.patch` from the project's root directory to `/path/to/virtualenv/lib/python2.7/site-packages/MySQLdb` and switch to this directory.

* Run `patch < times.patch` (sorry, fellow Windows users, next time).

### Creating a MySQL database

* Start MySQL server and log in using your credentials, e.g. `mysql -u root`
* Create a new database: `CREATE DATABASE mining;`
* Create a dedicated user to communicate with this database: `CREATE USER 'mining'@'localhost' IDENTIFIED BY 'mining';`
* And eventually give him that ring to rule them all: `GRANT ALL PRIVILEGES ON mining.* TO 'mining'@'localhost';`

## Usage 

The project consists of two parts: a performance testing repository and the analysis module.

To work with the repository, first create the needed entities (environment, counters, nodes, tests, test runs), then add data files with your test results to the test runs. You can use the example runs available at `data/` directory to see how it works. After you install the database, several empty test runs will already be created for you and you will only need to associate them with proper data files.

Use `/settings` page to fine-tune the algorithm settings.

Refer to Chapter 4 of the thesis document for more detailed usage instructions.

## References

A stand-alone version of the Dasha algorithm used to compare transaction databases and report differences in patterns between them is available at [python-dasha](https://bitbucket.org/dzzh/python-dasha) repository.

The FP-growth algorithm used in the Dasha algorithm to find frequent itemsets is based on the [implementation](https://github.com/enaeseth/python-fp-growth) by Eric Naeseth.

## License

`Performance repositories mining` is available under the terms of MIT License.

Copyright © 2013 [Źmicier Žaleźničenka][me]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

[me]: http://bitbucket.org/dzzh/
