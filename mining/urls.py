from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings

admin.autodiscover()

#Main page
urlpatterns = patterns('repo.views',
    url(r'^$', TemplateView.as_view(template_name='repo/index.html'), {'title': _('Home')}, 'site_index'),
)

#Authentication
urlpatterns += patterns('',
    url(r'^accounts/', include('allauth.urls')),
)

#Admin system
urlpatterns += patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

#Settings
urlpatterns += patterns('',
    url(r'^settings/', include('livesettings.urls')),
)

#Performance repository management
urlpatterns += patterns('',
    url(r'^repo/', include('repo.urls')),
)

urlpatterns += patterns('',
    url(r'^ajax/repo/', include('repo.urls_ajax')),
)

#Data analysis
urlpatterns += patterns('',
    url(r'^analysis/', include('analysis.urls')),
)

#Temporary hack for easier processing static files during development
urlpatterns += staticfiles_urlpatterns() + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
