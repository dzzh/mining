import datetime
from django.core import exceptions
from django.utils import timezone, six
from django.utils.dateparse import datetime_re
from django.utils.timezone import utc
from django.utils.tzinfo import FixedOffset
import warnings
from django.db.models import DateTimeField
from django.utils.translation import ugettext as _
from mining import settings


class PreciseDateTimeField(DateTimeField):
    """
    DateTimeField supporting sub-second precision up to microseconds
    """
    empty_strings_allowed = False
    description = _("Date with time and sub-second precision")

    def __init__(self, precision=0, verbose_name=None, name=None, auto_now=False,
                 auto_now_add=False, **kwargs):
        DateTimeField.__init__(self, verbose_name, name, auto_now, auto_now_add, **kwargs)

        if not 0 <= precision <= 6:
            raise ValueError('Sub-second precision must be in range from 0 to 6.')
        self.precision = precision

    def get_internal_type(self):
        return "PreciseDateTimeField"

    def db_type(self, connection):
        return 'datetime(%d)' % self.precision

    def to_python(self, value):
        """
        Copied from DateTimeField to point to different implementation of parse_datetime()
        """
        if value is None:
            return value
        if isinstance(value, datetime.datetime):
            return value
        if isinstance(value, datetime.date):
            value = datetime.datetime(value.year, value.month, value.day)
            if settings.USE_TZ:
                # For backwards compatibility, interpret naive datetimes in
                # local time. This won't work during DST change, but we can't
                # do much about it, so we let the exceptions percolate up the
                # call stack.
                warnings.warn("DateTimeField received a naive datetime (%s)"
                              " while time zone support is active." % value,
                              RuntimeWarning)
                default_timezone = timezone.get_default_timezone()
                value = timezone.make_aware(value, default_timezone)
            return value

        try:
            parsed = self.parse_datetime(value)
            if parsed is not None:
                return parsed
        except ValueError:
            msg = self.error_messages['invalid_datetime'] % value
            raise exceptions.ValidationError(msg)

        try:
            parsed = self.parse_date(value)
            if parsed is not None:
                return datetime.datetime(parsed.year, parsed.month, parsed.day)
        except ValueError:
            msg = self.error_messages['invalid_date'] % value
            raise exceptions.ValidationError(msg)

        msg = self.error_messages['invalid'] % value
        raise exceptions.ValidationError(msg)

    def parse_datetime(self, value):
        """
        Copied from django.utils.dateparse, adjusted for correct treatment of sub-second precision

        Parses a string and return a datetime.datetime.

        This function supports time zone offsets. When the input contains one,
        the output uses an instance of FixedOffset as tzinfo.

        Raises ValueError if the input is well formatted but not a valid datetime.
        Returns None if the input isn't well formatted.
        """
        match = datetime_re.match(value)
        if match:
            kw = match.groupdict()
            #            if kw['microsecond']:
            #                kw['microsecond'] = kw['microsecond'].ljust(6, '0')
            tzinfo = kw.pop('tzinfo')
            if tzinfo == 'Z':
                tzinfo = utc
            elif tzinfo is not None:
                offset = 60 * int(tzinfo[1:3]) + int(tzinfo[-2:])
                if tzinfo[0] == '-':
                    offset = -offset
                tzinfo = FixedOffset(offset)
            kw = dict((k, int(v)) for k, v in six.iteritems(kw) if v is not None)
            kw['tzinfo'] = tzinfo
            return datetime.datetime(**kw)

    def get_prep_value(self, value):
        value = self.to_python(value)
        if value is not None and settings.USE_TZ and timezone.is_naive(value):
            # For backwards compatibility, interpret naive datetimes in local
            # time. This won't work during DST change, but we can't do much
            # about it, so we let the exceptions percolate up the call stack.
            warnings.warn("DateTimeField received a naive datetime (%s)"
                          " while time zone support is active." % value,
                          RuntimeWarning)
            default_timezone = timezone.get_default_timezone()
            value = timezone.make_aware(value, default_timezone)
        return value

    def get_db_prep_value(self, value, connection, prepared=False):
        """
        Copied from DateTimeField to point to different implementation of value_to_db_datetime()
        """
        # Casts datetimes into the format expected by the backend
        if not prepared:
            value = self.get_prep_value(value)
        return self.value_to_db_datetime(value)

    def value_to_db_datetime(self, value):
        """
        Copied from django.db.backends.mysql.base to adjust for sub-second precision
        """
        if value is None:
            return None

        # MySQL doesn't support tz-aware datetimes
        if timezone.is_aware(value):
            if settings.USE_TZ:
                value = value.astimezone(timezone.utc).replace(tzinfo=None)
            else:
                raise ValueError("MySQL backend does not support timezone-aware datetimes when USE_TZ is False.")
        return six.text_type(value)
