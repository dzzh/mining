from operator import itemgetter
from django.shortcuts import get_object_or_404
import math
from matplotlib import gridspec
from matplotlib.figure import Figure
from matplotlib.pyplot import setp
from repo import REPO_CLASSIFIER
from repo.models import Test, TestRun, CounterRecordClass, RawCounterRecord, ConsolidatedCounterRecord
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from django.utils.translation import ugettext as _

#At least this number of percents should be between category values not to skip them while outputting.
#If setting too low, the values will overlap.
PERCENTS_BETWEEN_VALUES = 3

#Colors of the baseline lines at time-series chart
TIMESERIES_BASELINE_COLORS = ['#009933', '#FF9900', '#CC66FF', '#669999', '#996633']

#Color of the target line in time-series chart
TIMESERIES_TARGET_COLOR = '#0000FF'


def repo_chart_timeseries(test_id, testrun_id, counter_class_id, raw=False):
    """
    Create a cumulative chart containing time-series and box-and-whiskers plots.
    @param int test_id: id of current Test
    @param int testrun_id: id of interesting TestRun (either baseline or target)
    @param int counter_class_id: id of interesting CounterRecordClass
    @param bool raw: create chart for raw values if True, otherwise for discrete values
    @return FigureCanvas: chart canvas
    """
    test = get_object_or_404(Test, pk=test_id)
    testrun = get_object_or_404(TestRun, pk=testrun_id)
    counter = get_object_or_404(CounterRecordClass, pk=counter_class_id)

    if raw:
        model = RawCounterRecord
        title = _('Time series for raw values of counter %s' % counter.__unicode__())
    else:
        model = ConsolidatedCounterRecord
        title = _('Time series for consolidated values of counter %s' % counter.__unicode__())

    baseline_data = []
    for trid in test.get_baseline_testrun_ids():
        #If we only draw baseline data, we output specified baseline run separately as target
        if not trid == testrun_id:
            data = model.get_values_for_class([trid], counter, False)
            baseline_data.append((trid, data))

    target_data = (testrun_id, model.get_values_for_class([testrun], counter, False))

    classifier = REPO_CLASSIFIER(testrun)
    catnames = classifier.categories
    catvalues = classifier.get_category_boundaries(counter.id)

    return repo_chart_timeseries_canvas(baseline_data, target_data, catnames, catvalues, title, testrun.is_baseline)


def repo_chart_timeseries_canvas(baseline, target, catnames, catvalues, title, baseline_only):
    """
    Create canvas for the time-series chart.
    @param [(int, [])] baseline: list of tuples - (baseline id, list of values)
    @param (int, []) target: tuple - (target id, list of values)
    @param [] catnames: list of category names
    @param [] catvalues: list of category boundary values of size catnames-1
    @param str title: chart title
    @param bool baseline_only: if True, we draw baselines and target is interesting baseline,
    otherwise baselines and target
    @return FigureCanvas canvas
    """
    #Prepare canvas for drawing
    fig = Figure(figsize=(16, 4), dpi=80)
    gs = gridspec.GridSpec(1, 2, width_ratios=[4, 1])
    ts = fig.add_subplot(gs[0])
    bp = fig.add_subplot(gs[1])
    fig.subplots_adjust(wspace=0.3)

    #Prepare chart labels
    ts.set_title(title)
    bp.set_title(_('Box and Whisker'))

    ts.set_xlabel(_('Samples'))
    ts.set_ylabel(_('Value'))
    bp.set_ylabel(_('Value'))

    if baseline_only:
        setp(bp, xticklabels=[_('Baseline (%s)' % target[0]), _('Rest')])
    else:
        setp(bp, xticklabels=[_('Target'), _('Baseline')])

    if baseline_only:
        target_label = _('Baseline (%s)' % target[0])
    else:
        target_label = _('Target (%s)' % target[0])

    #Draw charts
    all_baseline_values = []  # merge all baseline values into one array that becomes boxplot input
    sorted_baseline = sorted(baseline, key=itemgetter(0))
    for index, data in enumerate(sorted_baseline):
        bsl_id, bsl_data = data[0], data[1]
        all_baseline_values += bsl_data

        #Draw baseline line at time-series chart
        color = TIMESERIES_BASELINE_COLORS[index % len(TIMESERIES_BASELINE_COLORS)]
        ts.plot(range(len(bsl_data)), bsl_data, label=_('Baseline (%s)' % bsl_id), color=color)

    #Draw target line at time-series chart
    ts.plot(range(len(target[1])), target[1], label=target_label, linewidth=2, color=TIMESERIES_TARGET_COLOR)

    #Draw boxplot
    bp.boxplot([target[1], all_baseline_values], widths=[0.65, 0.65])

    #Time-series: draw horizontal lines with category levels and assigning labels to them
    cat_labels = get_category_labels(catnames, catvalues, ts.axis())
    xmax = ts.axis()[1]

    for value in catvalues:
        ts.axhline(value, c='grey', ls='--')

    for label, value in cat_labels:
        #Print category labels at 1%+1 from the xmax by x axis
        ts.annotate(label, xy=(xmax, value), xytext=(xmax + int(xmax / 100) + 1, value), va='center')

    #Apply labels and annotations
    handles, labels = ts.get_legend_handles_labels()
    ts.legend(handles, labels, fontsize='small')

    return FigureCanvas(fig)


def get_category_labels(catnames, catvalues, axis):
    """
    Return list of categories with the positions to place them on the chart
    @param [] catnames: list of category names
    @param [] catvalues: list of category boundary values of size catnames-1
    @param axis: chart axis
    @return [str, float] list of labels and y-positions
    """
    ymin = axis[2]
    ymax = axis[3]
    labels = []

    #Sometimes axis does not return correct values, here we try to overcome it.
    #The problem is axis considers only values, but not axhlines when calculates axis.
    if ymin > catvalues[0]:
        lowest = catvalues[0]
    else:
        lowest = ymin

    if ymax < catvalues[-1]:
        highest = catvalues[-1]
    else:
        highest = ymax

    #Needed to prevent labels to overlap
    mindiff = math.fabs((highest - lowest) / 100 * PERCENTS_BETWEEN_VALUES)

    #Fill labels list with special treatment for the first and last values
    for i, _ in enumerate(catvalues):
        if i == 0:
            value = (lowest + catvalues[0]) / 2
        else:
            value = (catvalues[i - 1] + catvalues[i]) / 2
        labels.append((catnames[i], value))
    last_value = (catvalues[-1] + highest) / 2
    labels.append((catnames[-1], last_value))

    #Remove the labels that are too close
    last_printed_index = 0
    for i, value in enumerate(labels[:-1]):
        if math.fabs(labels[i + 1][1] - labels[last_printed_index][1]) < mindiff:
            #If label is too close to the previously printed, remove its label
            labels[i + 1] = ('', labels[i + 1][1])
        else:
            last_printed_index = i + 1

    return labels