from cache_utils.decorators import cached
from livesettings import config_value
import os
from django.core.urlresolvers import reverse
from django.db import models

from django.utils.translation import ugettext as _
from pytz import timezone
from mining import settings
from repo import ConverterParametersCsv, ConverterParametersXmlJmeter
from repo import REPO_CLASSIFIER
from precise_datetime import PreciseDateTimeField
#noinspection PyUnresolvedReferences
import config


class Environment(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('repo_env_details', args=[str(self.id)])

    def get_test_ids(self):
        return Test.objects.filter(environment__id=self.id).values_list('id', flat=True)

    def test_files(self):
        result = []
        tests = self.test_set.all()
        for test in tests:
            result += test.test_files()
        return result


class Counter(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    is_node_related = models.BooleanField(default=False,
        help_text=_('True for node-related counters, i.e. CPU, False for environment-related, i.e. response time'))

    def __unicode__(self):
        return self.name


class Node(models.Model):
    name = models.CharField(max_length=100)
    role = models.CharField(max_length=200, blank=True)
    environment = models.ForeignKey(Environment)

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.environment.name)

    def get_absolute_url(self):
        return reverse('repo_node_details', args=[str(self.environment.id), str(self.id)])


class CounterRecordClass(models.Model):
    label = models.CharField(max_length=200, db_index=True)
    counter = models.ForeignKey(Counter)

    def __unicode__(self):
        return '%(label)s - %(counter)s' % {'label': self.label, 'counter': self.counter}

    class Meta():
        unique_together = (('label', 'counter'),)
        ordering = ('label', 'counter')


class Test(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    environment = models.ForeignKey(Environment)
    sampling_period = models.IntegerField(
        help_text=_('Unified sampling period for all test runs and counters, in seconds'))
    disabled_counters = models.ManyToManyField(CounterRecordClass, blank=True)

    def __unicode__(self):
        return '%(test)s at %(environment)s' \
               % {'test': self.name, 'environment': self.environment.name}

    def get_absolute_url(self):
        return reverse('repo_test_details', args=[str(self.environment.id), str(self.id)])

    def test_files(self):
        result = []
        testruns = self.testrun_set.all()
        for run in testruns:
            result += run.test_files()
        return result

    def get_baseline_testrun_ids(self, limit=False):
        """
        Return ids of baseline test runs for the test ordered in descending time order.
        @param boolean limit: limit number of runs by REPO_MAX_TEST_RUNS_FOR_BASELINE.
        @return [int]: list of ids
        """
        return [t.id for t in self.get_baseline_testruns(limit)]

    def get_baseline_testruns(self, limit=False):
        """
        Return list of baseline test runs for the test ordered in descending time order.
        @param boolean limit: limit number of runs by REPO_MAX_TEST_RUNS_FOR_BASELINE.
        @return [TestRun]: list of test runs
        """
        testruns = self.testrun_set.filter(is_baseline=True).order_by('time_end').reverse()
        max_baseline_runs = config_value('repo_model', 'MAX_TEST_RUNS_FOR_BASELINE')
        if limit and max_baseline_runs > 0:
            return testruns[:max_baseline_runs]
        else:
            return testruns

    def get_target_testruns(self):
        """
        Return list of target test runs for the test ordered in descending time order.
        @return [TestRun]: list of test runs
        """
        return self.testrun_set.filter(is_baseline=False).order_by('time_end').reverse()

    def get_raw_baseline_counter_values_for_class_id(self, clazz, is_sorted=True):
        baseline_ids = self.get_baseline_testrun_ids()
        return RawCounterRecord.get_values_for_class(baseline_ids, clazz, is_sorted)

    def get_consolidated_baseline_counter_values_for_class_id(self, clazz, is_sorted=True):
        baseline_ids = self.get_baseline_testrun_ids()
        return ConsolidatedCounterRecord.get_values_for_class(baseline_ids, clazz, is_sorted)

    def get_baseline_transactions(self):
        result = []
        baseline_runs = self.testrun_set.filter(is_baseline=True).order_by('time_end').reverse()
        for run in baseline_runs:
            transactions = run.get_transactions()
            result += transactions
        return result


class TestRun(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    test = models.ForeignKey(Test)
    time_start = models.DateTimeField()
    time_end = models.DateTimeField()
    is_baseline = models.BooleanField(default=False)

    def __unicode__(self):
        return '%(testrun)s run (%(test)s)' \
               % {'testrun': self.name, 'test': self.test.name}

    def get_absolute_url(self):
        return reverse('repo_testrun_details', args=[str(self.test.environment.id),
                                                     str(self.test.id),
                                                     str(self.id)])

    def remove_raw_counter_records(self):
        RawCounterRecord.objects.filter(testrun__id=self.id).delete()

    def remove_consolidated_counter_records(self):
        ConsolidatedCounterRecord.objects.filter(testrun__id=self.id).delete()

    def get_formatted_time(self, dt):
        return dt.astimezone(timezone(settings.TIME_ZONE)).strftime(config_value('repo_model', 'DATE_TIME_FORMAT_MIN'))

    def formatted_start(self):
        return self.get_formatted_time(self.time_start)

    def formatted_end(self):
        return self.get_formatted_time(self.time_end)

    @cached(settings.CACHE_TIMEOUT)
    def get_transactions(self):
        """
        Return list of transactions, where each transaction is a list
        of consolidated performance counters with same offset.
        Array index is equal to the offset
        Records for disabled counters are not returned
        @return [[ConsolidatedCounterRecord]]
        """
        disabled_counter_classes = CounterRecordClass.objects. \
            filter(test__id__exact=self.test.id).values_list('id', flat=True)

        records = ConsolidatedCounterRecord.objects \
            .select_related('clazz__label', 'clazz__counter__name') \
            .filter(testrun__id=self.id) \
            .exclude(clazz__id__in=disabled_counter_classes) \
            .order_by('offset', 'clazz__label', 'clazz__counter__name')

        classifier = REPO_CLASSIFIER(testrun=self)
        transactions = []
        current_transaction = []
        current_offset = 0

        for record in records:
            if record.offset != current_offset:
                transactions.append(current_transaction)
                current_transaction = []
                current_offset += 1
            current_transaction.append(record)
        transactions.append(current_transaction)
        [classifier.classify(t) for t in transactions]

        return transactions

    def get_valid_classes(self):
        """
        @return [int] ids: list of raw counter record class ids that are used as identifiers
        for the associated counter records
        """
        return list(RawCounterRecord.objects.
            filter(testrun_id=self.id).order_by().values_list('clazz_id', flat=True).distinct())

    def test_files(self):
        return list(self.testfile_set.all())


class TestFile(models.Model):
    testrun = models.ForeignKey(TestRun)
    file = models.FileField(upload_to='test_files')
    uploaded_at = models.DateTimeField()
    original_name = models.CharField(max_length=254)

    def __unicode__(self):
        result = os.path.join(self.testrun.name, self.original_name)
        if self.uploaded_at:
            result += ' : ' + self.uploaded_at.strftime(config_value('repo_model', 'DATE_TIME_FORMAT_MIN'))
        return result

    def get_absolute_url(self):
        return reverse('repo_testfile_details', args=[str(self.testrun.test.environment.id),
                                                      str(self.testrun.test.id),
                                                      str(self.testrun.id),
                                                      str(self.id)])

    def get_parameters_for_jmeter_csv(self):
        parameters = ConverterParametersCsv(self)
        parameters.testrun_id = self.testrun.id
        parameters.testrun_start = self.testrun.time_start
        parameters.testrun_end = self.testrun.time_end
        parameters.node_related = False
        parameters.timestamp_column = 'timeStamp'
        parameters.label_column = 'label'
        latency_counter_id = Counter.objects.get(name='Latency').id
        parameters.counters.append((latency_counter_id, 'Latency'))
        return parameters

    def get_parameters_for_jmeter_xml(self):
        parameters = ConverterParametersXmlJmeter(self)
        parameters.testrun_id = self.testrun_id
        parameters.testrun_start = self.testrun.time_start
        parameters.testrun_end = self.testrun.time_end
        parameters.node_related = False
        latency_counter_id = Counter.objects.get(name='Latency').id
        parameters.counters.append((latency_counter_id, '@lt'))
        return parameters

    def get_parameters_for_opentsdb(self):
        parameters = ConverterParametersCsv(self)
        parameters.testrun_id = self.testrun.id
        parameters.testrun_start = self.testrun.time_start
        parameters.testrun_end = self.testrun.time_end
        parameters.node_related = True
        parameters.node_id = 1
        parameters.timestamp_column = 'timestamp'
        latency_counter_id = Counter.objects.get(name='CPU user').id
        parameters.counters.append((latency_counter_id, 'proc-cpu-user'))
        latency_counter_id = Counter.objects.get(name='CPU system').id
        parameters.counters.append((latency_counter_id, 'proc-cpu-system'))
        latency_counter_id = Counter.objects.get(name='CPU iowait').id
        parameters.counters.append((latency_counter_id, 'proc-cpu-iowait'))
        latency_counter_id = Counter.objects.get(name='MySQL: received bytes').id
        parameters.counters.append((latency_counter_id, 'mysql-received'))
        latency_counter_id = Counter.objects.get(name='MySQL: sent bytes').id
        parameters.counters.append((latency_counter_id, 'mysql-sent'))
        return parameters

    def get_parameters_for_opentsdb_dell(self):
        parameters = ConverterParametersCsv(self)
        parameters.testrun_id = self.testrun.id
        parameters.testrun_start = self.testrun.time_start
        parameters.testrun_end = self.testrun.time_end
        parameters.node_related = True
        parameters.node_id = 1
        parameters.timestamp_column = 'timestamp'
        counter_id = Counter.objects.get(name='CPU user').id
        parameters.counters.append((counter_id, 'proc-cpu-user'))
        counter_id = Counter.objects.get(name='CPU system').id
        parameters.counters.append((counter_id, 'proc-cpu-system'))
        counter_id = Counter.objects.get(name='CPU iowait').id
        parameters.counters.append((counter_id, 'proc-cpu-iowait'))
        counter_id = Counter.objects.get(name='MySQL: received bytes').id
        parameters.counters.append((counter_id, 'mysql-received'))
        counter_id = Counter.objects.get(name='MySQL: sent bytes').id
        parameters.counters.append((counter_id, 'mysql-sent'))
        counter_id = Counter.objects.get(name='CPU interrupts').id
        parameters.counters.append((counter_id, 'proc-stat-intr'))
        counter_id = Counter.objects.get(name='CPU context switches').id
        parameters.counters.append((counter_id, 'proc-stat-ctxt'))
        counter_id = Counter.objects.get(name='Disk read requests').id
        parameters.counters.append((counter_id, 'iostat-disk-read-requests'))
        counter_id = Counter.objects.get(name='Disk write requests').id
        parameters.counters.append((counter_id, 'iostat-disk-write-requests'))
        return parameters

    def num_distinct_counters(self):
        return RawCounterRecord.objects \
            .filter(testrun__id=self.testrun.id, file__id=self.id) \
            .values_list('clazz_id').distinct().count()

    def num_raw_records(self):
        return RawCounterRecord.objects.filter(file__id=self.id).count()

    def num_records(self):
        return ConsolidatedCounterRecord.objects.filter(file__id=self.id).count()

    def delete_all_records(self):
        RawCounterRecord.objects.filter(file__id=self.id).delete()
        ConsolidatedCounterRecord.objects.filter(file__id=self.id).delete()


class ConsolidatedCounterRecord(models.Model):
    clazz = models.ForeignKey(CounterRecordClass)
    testrun = models.ForeignKey(TestRun)
    offset = models.IntegerField()
    value = models.FloatField(null=True)
    file = models.ForeignKey(TestFile)
    category = None  # is set dynamically using classifiers

    def __unicode__(self):
        return '%(testrun)s : %(name)s : %(offset)d' % {'testrun': self.testrun.name, 'name': self.clazz.label,
                                                        'offset': self.offset}

    def get_absolute_url(self):
        return reverse('repo_consolidated_counter_record_details', args=[str(self.testrun.test.environment.id),
                                                                         str(self.testrun.test.id),
                                                                         str(self.testrun.id),
                                                                         str(self.id)])

    @staticmethod
    def get_values_for_class(testrun_ids, class_id, is_sorted=True):
        values = ConsolidatedCounterRecord.objects \
            .select_related('testrun') \
            .filter(testrun_id__in=testrun_ids, clazz_id=class_id) \
            .values_list('value', flat=True)
        if is_sorted:
            values = values.order_by('value')
        return list(values)

    class Meta():
        unique_together = (('testrun', 'offset', 'clazz'),)
        ordering = ('testrun', 'offset', 'clazz')


class RawCounterRecord(models.Model):
    clazz = models.ForeignKey(CounterRecordClass)
    testrun = models.ForeignKey(TestRun)
    file = models.ForeignKey(TestFile)
    timestamp = PreciseDateTimeField(3)
    value = models.FloatField(db_index=True)

    def __unicode__(self):
        return '%(testrun)s : %(counter)s : %(name)s : %(date)s' \
               % {'testrun': self.testrun.name, 'counter': self.clazz.counter.name, 'name': self.clazz.label,
                  'date': self.get_formatted_datetime()}

    def get_absolute_url(self):
        return reverse('repo_raw_counter_record_details', args=[str(self.testrun.test.environment.id),
                                                                str(self.testrun.test.id),
                                                                str(self.testrun.id),
                                                                str(self.id)])

    def get_formatted_datetime(self):
        return self.timestamp.astimezone(timezone(settings.TIME_ZONE)).strftime(
            config_value('repo_model', 'DATE_TIME_FORMAT_MS'))

    @staticmethod
    def get_values_for_class(testrun_ids, class_id, is_sorted=True):
        values = RawCounterRecord.objects \
            .select_related('testrun') \
            .filter(testrun_id__in=testrun_ids, clazz_id=class_id) \
            .values_list('value', flat=True)
        if is_sorted:
            values = values.order_by('value')
        return list(values)

    class Meta():
        ordering = ('testrun', 'timestamp', 'clazz')