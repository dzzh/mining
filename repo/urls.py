from django.conf.urls import patterns, url
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from repo.views import EnvironmentList, CounterList, EnvironmentDetail, CounterDetail, NodeDetail, NodeList, TestFileProcessCsvOpenTSDBDell, TestFileProcessXmlJmeter
from repo.views import TestFileProcessCsvOpenTSDB, EnvironmentDelete, NodeDelete, TestDelete, TestRunDelete, CounterDelete
from repo.views import TestDetail, TestList, TestRunDetail, TestRunList, RawCounterRecordList
from repo.views import RawCounterRecordDetail, CounterRecordDetail, CounterRecordList, TestFileList
from repo.views import TestFileDetail, TestFileDelete, TestFileFormatChooser, TestFileProcessCsv
from repo.views import RawCounterRecordDeleteAll, CounterRecordDeleteAll, TestFileProcessCsvJmeter

urlpatterns = patterns('',

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/(?P<counter>\d+)/rawChart.png$',
        'repo.views.repo_chart_timeseries_raw', {}, 'repo_chart_timeseries_raw'),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/(?P<counter>\d+)/discreteChart.png$',
        'repo.views.repo_chart_timeseries_discrete', {}, 'repo_chart_timeseries_discrete'),

    url(r'environments/(?P<pk>\d+)/delete/$', EnvironmentDelete.as_view(), {}, 'repo_environment_delete'),

    url(r'environments/(?P<pk>\d+)/$', EnvironmentDetail.as_view(), {}, 'repo_environment_details'),

    url(r'environments/$', EnvironmentList.as_view(), {}, "repo_environments"),

    url(r'counters/(?P<pk>\d+)/delete/$', CounterDelete.as_view(), {}, "repo_counter_delete"),

    url(r'counters/(?P<pk>\d+)/$', CounterDetail.as_view(), {}, "repo_counter_details"),

    url(r'counters/$', CounterList.as_view(), {}, "repo_counters"),

    url(r'environments/(?P<environment>\d+)/nodes/(?P<pk>\d+)/delete/$', NodeDelete.as_view(), {}, "repo_node_delete"),

    url(r'environments/(?P<environment>\d+)/nodes/(?P<pk>\d+)/$', NodeDetail.as_view(), {}, "repo_node_details"),

    url(r'environments/(?P<environment>\d+)/nodes/$', NodeList.as_view(), {}, "repo_nodes"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<pk>\d+)/delete/$', TestDelete.as_view(), {}, 'repo_test_delete'),

    url(r'environments/(?P<environment>\d+)/tests/(?P<pk>\d+)/$', TestDetail.as_view(), {}, "repo_test_details"),

    url(r'environments/(?P<environment>\d+)/tests/$', TestList.as_view(), {}, "repo_tests"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<pk>\d+)/delete/$',
        TestRunDelete.as_view(), {}, "repo_testrun_delete"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<pk>\d+)/$',
        TestRunDetail.as_view(), {}, "repo_testrun_details"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/$',
        TestRunList.as_view(), {}, "repo_testruns"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/$',
        TestFileList.as_view(), {}, "repo_test_files"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/$',
        TestFileDetail.as_view(), {}, "repo_test_file_details"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/delete/$',
        TestFileDelete.as_view(), {}, "repo_test_file_delete"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/$',
        TestFileFormatChooser.as_view(), {}, "repo_test_file_process"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/error/$',
        TemplateView.as_view(template_name='repo/test_file_process_error.html'),
        {'title': _('An error has occurred')}, 'repo_test_file_process_error'),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/csv/jmeter/$',
        TestFileProcessCsvJmeter.as_view(), {}, "repo_test_file_process_csv_jmeter"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/xml/jmeter/$',
        TestFileProcessXmlJmeter.as_view(), {}, "repo_test_file_process_xml_jmeter"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/csv/opentsdb_dell/$',
        TestFileProcessCsvOpenTSDBDell.as_view(), {}, "repo_test_file_process_csv_opentsdbdell"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/csv/opentsdb/$',
        TestFileProcessCsvOpenTSDB.as_view(), {}, "repo_test_file_process_csv_opentsdb"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/files/(?P<pk>\d+)/process/csv/(?P<format>\w+)/$',
        TestFileProcessCsv.as_view(), {}, "repo_test_file_process_csv_generic"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/rawcounterrecords/$',
        RawCounterRecordList.as_view(), {}, "repo_raw_counter_records"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/rawcounterrecords/delete/$',
        RawCounterRecordDeleteAll.as_view(), {}, "repo_raw_counter_records_delete"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/rawcounterrecords/(?P<pk>\d+)/$',
        RawCounterRecordDetail.as_view(), {}, "repo_raw_counter_records_details"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/consolidatedcounterrecords/$',
        CounterRecordList.as_view(), {}, "repo_consolidated_counter_records"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/consolidatedcounterrecords/delete/$',
        CounterRecordDeleteAll.as_view(), {}, "repo_consolidated_counter_records_delete"),

    url(r'environments/(?P<environment>\d+)/tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/consolidatedcounterrecords/(?P<pk>\d+)/$',
        CounterRecordDetail.as_view(), {}, "repo_consolidated_counter_records_details"),

    url(r'$', TemplateView.as_view(template_name='repo/repo.html'),
        {'title': _('Performance testing repository')} ,"repo_index"),
)