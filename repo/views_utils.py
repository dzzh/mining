import math
from livesettings import config_value
from repo import REPO_SUMMARY_STAT_PARAMETERS


def prepare_summary(summary):
    """
    Prepare counter records summary data for output.
    """
    #This may occur if a target test contains counters not presented in the baseline runs.
    for entry in summary:
        if not 'violation_top' in entry:
            entry['violation_top'] = {'value': None}
            entry['violation_bottom'] = {'value': None}

    summary = sorted(summary, key=lambda entry: entry['violation_top']['value'], reverse=True)

    for entry in summary:
        for parameter in REPO_SUMMARY_STAT_PARAMETERS:
            if parameter in entry and 'difference' in entry[parameter]:
                entry[parameter]['difference'], entry[parameter]['classes'] = prepare_percent_value(
                    entry[parameter]['difference'])

        if entry['violation_top']['value'] is not None:
            entry['violation_top']['value'], entry['violation_top']['classes'] = prepare_percent_value(
                entry['violation_top']['value'])
            entry['violation_top']['value'] = entry['violation_top']['value'][1:]  # remove leading plus
            entry['violation_bottom']['value'], entry['violation_bottom']['classes'] = prepare_percent_value(
                entry['violation_bottom']['value'], True)
            entry['violation_bottom']['value'] = entry['violation_bottom']['value'][1:]  # remove leading plus
    return summary


def prepare_percent_value(value, reverse=False):
    """
    Prepare percentage value for displaying in the view: convert to str and specify classes.
    """
    if value is None:
        return None, ''

    #If we output '0.00%', we always consider it a success
    if -0.005 <= value <= 0.005:
        css = 'text-success'

    else:
        if value > 0:
            if not reverse:
                css = 'text-error'
            else:
                css = 'text-success'
        else:
            if not reverse:
                css = 'text-success'
            else:
                css = 'text-error'

    if math.fabs(value) > config_value('repo_view', 'SUMMARY_BOLD_THRESHOLD'):
        css += ' text-strong'

    return config_value('repo_view', 'SUMMARY_DIFF_FORMAT').format(value), css