import logging
import datetime
from pytz import timezone
from mining import settings
from repo.models import CounterRecordClass

UNIX_TIMESTAMP_LENGTH = 10
LJUST = 6


class RawConverter():
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.counter_record_classes = list(CounterRecordClass.objects.all())

    def timestamp_to_datetime(self, timestamp):
        """
        Convert timestamp to datetime. Properly work with timestamps with sub-second precision up to microseconds.
        """
        string = str(timestamp)
        length = len(string)
        if length > UNIX_TIMESTAMP_LENGTH:
            unix_ts = int(string[0:UNIX_TIMESTAMP_LENGTH])
            microseconds = int(string[UNIX_TIMESTAMP_LENGTH:].ljust(LJUST, '0'))
        else:
            unix_ts = int(timestamp)
            microseconds = 0

        dt = datetime.datetime.fromtimestamp(unix_ts, timezone(settings.TIME_ZONE))
        dt += datetime.timedelta(microseconds=microseconds)
        return dt

    def get_counter_record_class(self, name, counter):
        for clazz in self.counter_record_classes:
            if clazz.counter == counter and clazz.label == name:
                return clazz
        clazz = CounterRecordClass()
        clazz.label = name
        clazz.counter = counter
        clazz.save()
        self.counter_record_classes.append(clazz)
        return clazz