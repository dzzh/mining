import csv
import math
import time
from livesettings import config_value
from repo.converters.raw_converter import RawConverter
from repo.models import Node, TestRun, Counter, RawCounterRecord


class CsvConverter(RawConverter):
    def convert_to_raw_counter_records(self, parameters):
        """
        Convert counter values from .csv files to raw counter records.

        @param ConverterParametersCsv parameters: parameters specification
        """
        start = time.time()
        file_url = parameters.get_file_url()
        testrun = TestRun.objects.get(pk=parameters.testrun_id)
        label = ''
        if parameters.node_related:
            label = Node.objects.get(pk=parameters.node_id).name
        with open(file_url, 'r') as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(4096))
            csvfile.seek(0)
            reader = csv.DictReader(csvfile, dialect=dialect)
            records = []
            skipped_records = 0
            processed_records = 0
            processed_counter_values = 0
            counters = {}  # simple caching to decrease number of DB calls

            for index, row in enumerate(reader):

                #Write a set of records
                if index % config_value('repo_model', 'BATCH_INSERT_SIZE') == 0 and records:
                    RawCounterRecord.objects.bulk_create(records)
                    records = []

                #Create a new set

                #First check that the record is within the test run time frame
                dt = self.timestamp_to_datetime(row[parameters.timestamp_column])
                if not parameters.testrun_start <= dt <= parameters.testrun_end:
                    skipped_records += 1
                    continue

                for counter_tuple in parameters.counters:
                    record = RawCounterRecord()

                    if label:
                        name = label
                    else:
                        name = row[parameters.label_column]

                    if counter_tuple[0] in counters:
                        counter = counters[counter_tuple[0]]
                    else:
                        counter = Counter.objects.get(pk=counter_tuple[0])
                        counters[counter_tuple[0]] = counter

                    record.clazz = self.get_counter_record_class(name, counter)
                    record.file = parameters.file
                    record.testrun = testrun
                    record.timestamp = dt
                    record.value = float(row[counter_tuple[1]])
                    if not math.isnan(record.value):
                        records.append(record)
                        processed_counter_values += 1
                processed_records += 1

            #Write the rest
            RawCounterRecord.objects.bulk_create(records)

            if skipped_records:
                self.logger.info(
                    '%d records were not processed as they are outside the test run time frame.' % skipped_records)

            sec = time.time() - start
            self.logger.info('%d records with %d counter values were processed in %f seconds.' % (
                processed_records, processed_counter_values, sec))