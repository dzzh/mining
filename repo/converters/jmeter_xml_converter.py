import math
import time
from lxml import etree
from livesettings import config_value
from repo.converters.raw_converter import RawConverter
from repo.models import TestRun, Counter, RawCounterRecord


class XmlConverter(RawConverter):
    def convert_to_raw_counter_records(self, parameters):
        """
        Convert counter values from XML files to raw counter records.

        @param ConverterParametersXml parameters: parameters specification
        """
        start = time.time()
        file_url = parameters.get_file_url()
        testrun = TestRun.objects.get(pk=parameters.testrun_id)

        records = []
        skipped_records = 0
        processed_records = 0
        processed_counter_values = 0
        counters = {}  # simple caching to decrease number of DB calls

        data = etree.parse(file_url).getroot().xpath('//httpSample')

        for index, entry in enumerate(data):
            timestamp = entry.xpath('@ts')[0]
            label = entry.xpath('@lb')[0]

            #Write a set of records
            if index % config_value('repo_model', 'BATCH_INSERT_SIZE') == 0 and records:
                RawCounterRecord.objects.bulk_create(records)
                records = []

            #Create a new set

            #First check that the record is within the test run time frame
            dt = self.timestamp_to_datetime(timestamp)
            if not parameters.testrun_start <= dt <= parameters.testrun_end:
                skipped_records += 1
                continue

            for counter_tuple in parameters.counters:
                record = RawCounterRecord()

                if counter_tuple[0] in counters:
                    counter = counters[counter_tuple[0]]
                else:
                    counter = Counter.objects.get(pk=counter_tuple[0])
                    counters[counter_tuple[0]] = counter

                record.clazz = self.get_counter_record_class(label, counter)
                record.file = parameters.file
                record.testrun = testrun
                record.timestamp = dt
                record.value = float(entry.xpath(counter_tuple[1])[0])
                if not math.isnan(record.value):
                    records.append(record)
                    processed_counter_values += 1
            processed_records += 1

        #Write the rest
        RawCounterRecord.objects.bulk_create(records)

        if skipped_records:
            self.logger.info(
                '%d records were not processed as they are outside the test run time frame.' % skipped_records)

        sec = time.time() - start
        self.logger.info('%d records with %d counter values were processed in %f seconds.' % (
            processed_records, processed_counter_values, sec))