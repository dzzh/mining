from django.utils.translation import ugettext as _
from livesettings import ConfigurationGroup, config_register, PositiveIntegerValue, StringValue

REPO_MODEL_GROUP = ConfigurationGroup(
    'repo_model',
    _('Settings of the performance repository models')
)

REPO_CLASSIFIER_GROUP = ConfigurationGroup(
    'repo_classifier',
    _('Settings of the performance repository classifiers')
)

REPO_VIEW_GROUP = ConfigurationGroup(
    'repo_view',
    _('Settings of the performance repository views')
)

#Models-related settings

config_register(PositiveIntegerValue(
    REPO_MODEL_GROUP,
    'MAX_TEST_RUNS_FOR_BASELINE',
    description=_('Number of baseline test runs to use'),
    help_text=_('Maximal number of baseline test runs that will be used in calculations, 0 to consider all.'),
    ordering=1,
    default=3
))

config_register(PositiveIntegerValue(
    REPO_MODEL_GROUP,
    'BATCH_INSERT_SIZE',
    description=_('Num entries to insert in DB in one go'),
    help_text=_('Max number of entries to write to db at once. Is used for performance optimization.'),
    ordering=2,
    default=100
))

config_register(StringValue(
    REPO_MODEL_GROUP,
    'DATE_TIME_FORMAT_MIN',
    description=_('Short datetime format'),
    help_text=_('Short format of datetime used in models, up to minutes.'),
    ordering=3,
    default='%Y-%m-%d %H:%M'
))

config_register(StringValue(
    REPO_MODEL_GROUP,
    'DATE_TIME_FORMAT_MS',
    description=_('Long datetime format'),
    help_text=_('Long format of datetime used in models, up to microseconds.'),
    ordering=4,
    default='%Y-%m-%d %H:%M:%S.%f'
))

#Classifiers-related settings

config_register(PositiveIntegerValue(
    REPO_CLASSIFIER_GROUP,
    'FIXED_STDDEV_CLASSIFIER_THRESHOLD',
    description=_('Fixed-stddev classifier threshold %'),
    help_text=_('If value differs from median by stddev * threshold, it is considered to be outlier.'),
    ordering=1,
    default=100
))

config_register(PositiveIntegerValue(
    REPO_CLASSIFIER_GROUP,
    'FIXED_PERCENT_CLASSIFIER_THRESHOLD',
    description=_('Fixed-percent classifier threshold %'),
    help_text=_('This percentage of baseline values is considered to be outliers.'),
    ordering=2,
    default=2
))

#Views-related settings

config_register(PositiveIntegerValue(
    REPO_VIEW_GROUP,
    'PAGINATE_BY',
    description=_('Number of entries to show at one page'),
    help_text=_('Number of entries in a long list to be shown at most at one page.'),
    ordering=1,
    default=100
))

config_register(StringValue(
    REPO_VIEW_GROUP,
    'SUMMARY_DIFF_FORMAT',
    description=_('Difference display format'),
    help_text=_('Format of string to display relative difference in summary tables.'),
    ordering=2,
    default='{:+.2f}%'
))

config_register(PositiveIntegerValue(
    REPO_VIEW_GROUP,
    'SUMMARY_BOLD_THRESHOLD',
    description=_('Summary bold threshold'),
    help_text=_('If the relative difference in summary tables exceeds this value, it will be shown in bold.'),
    ordering=3,
    default=10
))