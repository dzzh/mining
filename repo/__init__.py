from repo.classifiers.percentile_classifiers import QuartileStddevClassifier

#Used classifier
REPO_CLASSIFIER = QuartileStddevClassifier

#Statistical parameters available in summary tables
REPO_SUMMARY_STAT_PARAMETERS = ['min', 'max', 'avg', 'median', 'stddev']

class ConverterParameters():

    def __init__(self, file):
        self.file = file
        self.testrun_id = None
        self.testrun_start = None
        self.testrun_end = None
        self.node_related = False
        self.node_id = None
        self.counters = []

    def get_file_url(self):
        return self.file.file._get_path()


class ConverterParametersCsv(ConverterParameters):

    def __init__(self, file):
        ConverterParameters.__init__(self, file)
        self.timestamp_column = None
        self.label_column = ''


class ConverterParametersXmlJmeter(ConverterParameters):

    def __init__(self, file):
        ConverterParameters.__init__(self, file)
        self.record_xpath = '//httpSample'
        self.timestamp_xpath = '@ts'
        self.label_xpath = '@lb'