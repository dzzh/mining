from cache_utils.decorators import cached
from django.db import connection
import numpy
from mining import settings
from repo import REPO_CLASSIFIER
from repo.models import ConsolidatedCounterRecord, RawCounterRecord, TestRun

QUERY_AGGREGATION = \
    'SELECT repo_consolidatedcounterrecord.clazz_id as clazz, ' \
    'repo_counterrecordclass.label, ' \
    'repo_counter.name AS counter_name, ' \
    'COUNT(*), MIN(value), MAX(value), AVG(value), STDDEV(value) ' \
    'FROM repo_consolidatedcounterrecord ' \
    'JOIN repo_counterrecordclass ON repo_consolidatedcounterrecord.clazz_id = repo_counterrecordclass.id ' \
    'JOIN repo_counter ON repo_counterrecordclass.counter_id = repo_counter.id ' \
    'WHERE testrun_id IN (%s) ' \
    'GROUP BY label, counter_name'

QUERY_AGGREGATION_RAW = \
    'SELECT repo_rawcounterrecord.clazz_id as clazz, ' \
    'repo_counterrecordclass.label, ' \
    'repo_counter.name AS counter_name, ' \
    'COUNT(*), MIN(value), MAX(value), AVG(value), STDDEV(value) ' \
    'FROM repo_rawcounterrecord ' \
    'JOIN repo_counterrecordclass ON repo_rawcounterrecord.clazz_id = repo_counterrecordclass.id ' \
    'JOIN repo_counter ON repo_counterrecordclass.counter_id = repo_counter.id ' \
    'WHERE testrun_id IN (%s) ' \
    'GROUP BY label, counter_name'


def num_outliers(values, min_val, max_val):
    """
    Return number of values in a list that don't lie within the [min,max] range
    @param [float] values: list of values
    @param float min_val: min value
    @param float max_val: max value
    @return (int, int): number of bottom outliers, number of top outliers
    """
    return len([i for i in values if i < min_val]), len([i for i in values if i > max_val])


def difference(target, baseline):
    """
    Return relative difference (in percents) between two floats.
    @param float target: target value
    @param float baseline: baseline value
    @return float: result
    """

    if baseline == 0:
        return 0
    return (target - baseline) / baseline * 100


@cached(settings.CACHE_TIMEOUT)
def get_summary_for_testruns(testrun_ids, baseline_testrun_ids=None, raw=True):
    """
    Return summary data for each valid class in a given test.

    Should not be used by a client. Instead, use function:: get_summary_for_testrun(testrun, baseline_runs, raw)
    to get summary for a single target run or use function:: get_baseline_summary(baseline_runs, raw)
    to retrieve summary for baseline runs.

    @param [int] testrun_ids: test run ids to retrieve summary data
    @param {int} baseline_testrun_ids: ids of test runs in baseline to perform comparison
    @param boolean raw: if true, calculate summary for raw counters, otherwise for consolidated counters
    @return [{}]: list of dictionaries
    """
    if not testrun_ids:
        return []

    testrun_ids_str = ', '.join([str(i) for i in testrun_ids])

    cursor = connection.cursor()
    #Get all used counters and their corresponding labels
    summary = []

    #Get aggregated data
    if raw:
        cursor.execute(QUERY_AGGREGATION_RAW % testrun_ids_str)
    else:
        cursor.execute(QUERY_AGGREGATION % testrun_ids_str)
    for entry in cursor.fetchall():

        e = {'clazz': entry[0], 'label': entry[1], 'counter_name': entry[2], 'num_samples': entry[3],
             'min': {'value': entry[4]}, 'max': {'value': entry[5]}, 'avg': {'value': entry[6]},
             'stddev': {'value': entry[7]}}

        #No convenient way to compute median directly in MySQL, so do it separately with Python
        if raw:
            e['values'] = list(RawCounterRecord.objects.filter(clazz=e['clazz'], testrun_id__in=testrun_ids).order_by(
                'value').values_list('value', flat=True))
        else:
            e['values'] = list(
                ConsolidatedCounterRecord.objects.filter(clazz=e['clazz'], testrun_id__in=testrun_ids).order_by(
                    'value').values_list('value', flat=True))
        e['median'] = {'value': numpy.median(e['values'])}

        summary.append(e)

    if baseline_testrun_ids:
        if len(testrun_ids) > 1:
            raise ValueError(
                'If you work with baseline runs, there should be a single target test run to calculate summary.')
        baseline_testrun_ids_str = ', '.join([str(i) for i in baseline_testrun_ids])
        classifier = REPO_CLASSIFIER(TestRun.objects.get(pk=testrun_ids[0]))
        if raw:
            cursor.execute(QUERY_AGGREGATION_RAW % baseline_testrun_ids_str)
        else:
            cursor.execute(QUERY_AGGREGATION % baseline_testrun_ids_str)
        for diff in cursor.fetchall():

            for e in summary:
                if e['clazz'] == diff[0]:
                    e['min']['difference'] = difference(e['min']['value'], diff[4])
                    e['max']['difference'] = difference(e['max']['value'], diff[5])
                    e['avg']['difference'] = difference(e['avg']['value'], diff[6])
                    e['stddev']['difference'] = difference(e['stddev']['value'], diff[7])

                    if raw:
                        base_values = list(RawCounterRecord.objects.
                            filter(clazz=e['clazz'], testrun_id__in=baseline_testrun_ids).
                            order_by('value').
                            values_list('value', flat=True))
                    else:
                        base_values = list(ConsolidatedCounterRecord.objects.
                                                filter(clazz=e['clazz'], testrun_id__in=baseline_testrun_ids).
                                                order_by('value').
                                                values_list('value', flat=True))

                    base_median = float(numpy.median(base_values))
                    e['median']['difference'] = difference(e['median']['value'], base_median)
                    lower_bound, upper_bound = classifier.get_outlier_boundaries(e['clazz'])
                    outliers_bottom, outliers_top = num_outliers(e['values'], lower_bound, upper_bound)
                    violation_top = float(outliers_top) / e['num_samples'] * 100
                    violation_bottom = float(outliers_bottom) / e['num_samples'] * 100
                    e['violation_top'] = {'value': violation_top}
                    e['violation_bottom'] = {'value': violation_bottom}

    #performance optimization
    for e in summary:
        e.pop('values')

    return summary


def get_summary_for_testrun(testrun, baseline_run_ids, raw=True):
    """
    Return summary data for a target run provided its baseline runs.

    @param int testrun: test run id to retrieve summary data
    @param [int] baseline_run_ids: ids of test runs in baseline to perform comparison
    @param boolean raw: if true, calculate summary for raw counters, otherwise for consolidated counters
    @return [{}]: list of dictionaries
    """
    return get_summary_for_testruns([testrun], set(baseline_run_ids), raw)


def get_baseline_summary(baseline_run_ids, raw=True):
    """
    Return composite summary data for a number of baseline runs.

    @param [int] baseline_run_ids: ids of test runs in baseline to perform comparison
    @param boolean raw: if true, calculate summary for raw counters, otherwise for consolidated counters
    @return [{}]: list of dictionaries
    """
    return get_summary_for_testruns(set(baseline_run_ids), None, raw)


def invalidate_summary(testrun, baseline_run_ids):
    """
    Invalidate cache for summary table
    @param int testrun: id of current testrun
    @param [int] baseline_run_ids: list of baseline runs
    """
    get_summary_for_testruns.invalidate([testrun], set(baseline_run_ids), True)
    get_summary_for_testruns.invalidate([testrun], set(baseline_run_ids), False)