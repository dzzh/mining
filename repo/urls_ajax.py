from django.conf.urls import url, patterns

urlpatterns = patterns('repo.views',
    url(r'change_counter_class_status/$',
        'ajax_change_counter_class_status', {}, 'repo_ajax_change_counter_class_status'),
)