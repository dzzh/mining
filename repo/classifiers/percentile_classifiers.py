#Classifiers that categorize data by mapping it to certain percentiles of baselines.
from livesettings import config_value
import numpy
from repo.classifiers.base_classifier import BaseClassifier


class QuartileClassifier(BaseClassifier):
    categories = ['Low', '1st', '2nd', '3rd', 'High']
    baselines = {}

    def __init__(self, testrun, *args, **kwargs):
        super(QuartileClassifier, self).__init__(*args, **kwargs)
        self.testrun = testrun
        #All the baselines for this test run are generated at init step to optimize performance
        self.calculate_baselines()

    def calculate_baselines(self):
        """
        For each valid class calculate quartile values.
        """
        valid_classes = self.testrun.get_valid_classes()
        for clazz in valid_classes:
            values = self.testrun.test.get_raw_baseline_counter_values_for_class_id(clazz)
            self.baselines[clazz] = (numpy.percentile(values, 0),
                                     numpy.percentile(values, 25),
                                     numpy.percentile(values, 75),
                                     numpy.percentile(values, 100))

    def classify(self, records, *args, **kwargs):
        """
        @param [ConsolidatedCounterRecord] records: records to classify
        @return [ConsolidatedCounterRecord]: classified records
        """
        for record in records:
            record.category = self.categories[-1]
            for index, category in enumerate(self.categories[:-1]):
                if record.value < self.baselines[record.clazz.id][index]:
                    record.category = category
                    break
        return records

    def get_outlier_boundaries(self, clazz_id):
        lower_bound = self.baselines[clazz_id][0]
        upper_bound = self.baselines[clazz_id][3]
        return lower_bound, upper_bound

    def get_category_boundaries(self, clazz_id):
        return list(self.baselines[clazz_id]) if clazz_id in self.baselines else []


class QuartileStddevClassifier(BaseClassifier):
    categories = ['Low', '1st', '2nd', '3rd', 'High']
    baselines = {}
    stats = {}

    def __init__(self, testrun, std_dev_percent=None, *args, **kwargs):
        super(QuartileStddevClassifier, self).__init__(*args, **kwargs)
        self.testrun = testrun
        self.std_dev_percent = std_dev_percent if std_dev_percent \
            else config_value('repo_classifier', 'FIXED_STDDEV_CLASSIFIER_THRESHOLD')
        #All the baselines for this test run are generated at init step to optimize performance
        self.calculate_baselines()

    def calculate_baselines(self):
        """
        For each valid class calculate quartile values.
        """
        valid_classes = self.testrun.get_valid_classes()
        for clazz in valid_classes:
            values = self.testrun.test.get_raw_baseline_counter_values_for_class_id(clazz)
            base_median = numpy.median(values)
            base_stddev = numpy.std(values)
            self.stats[clazz] = (base_median, base_stddev)
            lower_bound, upper_bound = self.get_outlier_boundaries(clazz)
            values_without_outliers = [v for v in values if lower_bound <= v <= upper_bound]
            self.baselines[clazz] = (lower_bound,
                                     numpy.percentile(values_without_outliers, 25),
                                     numpy.percentile(values_without_outliers, 75),
                                     upper_bound)

    def classify(self, records, *args, **kwargs):
        """
        @param [ConsolidatedCounterRecord] records: records to classify
        @return [ConsolidatedCounterRecord]: classified records
        """
        for record in records:
            record.category = self.categories[-1]
            for index, category in enumerate(self.categories[:-1]):
                if record.value < self.baselines[record.clazz.id][index]:
                    record.category = category
                    break
        return records

    def get_outlier_boundaries(self, clazz_id):
        base_median, base_stddev = self.stats[clazz_id]
        lower_bound = base_median - base_stddev * self.std_dev_percent / 100
        upper_bound = base_median + base_stddev * self.std_dev_percent / 100
        return lower_bound, upper_bound

    def get_category_boundaries(self, clazz_id):
        return list(self.baselines[clazz_id]) if clazz_id in self.baselines else []