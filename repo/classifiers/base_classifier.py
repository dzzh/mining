"""
An ABC for the real classifiers
"""
from abc import ABCMeta, abstractmethod


class BaseClassifier():
    __metaclass__ = ABCMeta

    categories = []

    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def classify(self, records, *args, **kwargs):
        """
        Take a list of consolidated counter records and classify them based on the baseline values and/or other data.
        @param [ConsolidatedCounterRecord] records: list of counter records to be classified
        @return [ConsolidatedCounterRecord]: classified list
        """
        pass

    @abstractmethod
    def get_outlier_boundaries(self, clazz_id):
        """
        Return a range used to check whether the value is an outlier. If the target value lies within a range, it is not
        @param int clazz_id: id of a counter record class
        @return (float, float): range
        """
        pass

    def get_categories(self):
        """
        Return a list of categories that can be associated with the values
        @return [str]: list of categories
        """
        return self.categories

    @abstractmethod
    def get_category_boundaries(self, clazz_id):
        """
        Return a list of values that are boundary between the categories.
        Has categories-1 values.
        @param int clazz_id: identifier of CounterRecordClass
        """
        pass