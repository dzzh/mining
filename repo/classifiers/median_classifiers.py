#Classifiers working with Low, Medium and High categories and are based on calculation of median value for raw counters
import math
from livesettings import config_value

import numpy
from repo.classifiers.base_classifier import BaseClassifier


class GenericMedianClassifier(BaseClassifier):
    """
    An abstract classifier operating with three categories: Low, Medium and High.
    """
    categories = ['Low', 'Medium', 'High']
    baselines = {}

    def __init__(self, testrun, *args, **kwargs):
        super(GenericMedianClassifier, self).__init__(*args, **kwargs)
        self.testrun = testrun
        #All the baselines for this test run are generated at init step to optimize performance
        self.calculate_baselines()

    def calculate_baselines(self):
        """
        For each valid class calculate median and std.dev.
        """
        valid_classes = self.testrun.get_valid_classes()
        for clazz in valid_classes:
            values = self.testrun.test.get_raw_baseline_counter_values_for_class_id(clazz)
            base_median = numpy.median(values)
            base_stddev = numpy.std(values)
            self.baselines[clazz] = (base_median, base_stddev)


class FixedStddevClassifier(GenericMedianClassifier):
    """
    A classifier working with fixed standard deviation value.
    """

    def __init__(self, testrun, std_dev_percent=None, *args, **kwargs):
        """
        @param TestRun testrun: test run
        @param int std_dev_percent: Percentage of standard deviation. If a value differs from a median
        by std.dev * percent, the value does not belong to Medium category. E.g. if percent = 100, then
        values within base_median +- 1 std.dev. are classified as Medium.
        """
        super(FixedStddevClassifier, self).__init__(testrun, *args, **kwargs)

        self.std_dev_percent = std_dev_percent if std_dev_percent \
            else config_value('repo_classifier', 'FIXED_STDDEV_CLASSIFIER_THRESHOLD')

    def classify(self, records, *args, **kwargs):
        """
        @param [ConsolidatedCounterRecord] records: records to classify
        @return [ConsolidatedCounterRecord]: classified records
        """
        for record in records:
            lower_bound, upper_bound = self.get_outlier_boundaries(record.clazz_id)

            if record.value < lower_bound:
                record.category = 'Low'
            elif record.value > upper_bound:
                record.category = 'High'
            else:
                record.category = 'Medium'
        return records

    def get_outlier_boundaries(self, clazz_id):
        base_median, base_stddev = self.baselines[clazz_id]
        lower_bound = base_median - base_stddev * self.std_dev_percent / 100
        upper_bound = base_median + base_stddev * self.std_dev_percent / 100
        return lower_bound, upper_bound

    def get_category_boundaries(self, clazz_id):
        base_median, base_stddev = self.baselines[clazz_id]
        lower_bound = base_median - base_stddev * self.std_dev_percent / 100
        upper_bound = base_median + base_stddev * self.std_dev_percent / 100
        return [lower_bound, upper_bound]


class FixedPercentClassifier(GenericMedianClassifier):
    """
    A classifier working with fixed percentage of outliers
    """
    boundaries = {}

    def __init__(self, testrun, percent=None, *args, **kwargs):
        """
        @param TestRun testrun: test run
        @param int percent: Percentage of outliers. For all baseline sets this amount of values is considered to be
        outliers. The rest of values is used to find boundaries for Medium category.
        """
        super(FixedPercentClassifier, self).__init__(testrun, *args, **kwargs)

        self.percent = percent if percent else config_value('repo_classifier', 'FIXED_PERCENT_CLASSIFIER_THRESHOLD')

        valid_classes = self.testrun.get_valid_classes()
        for clazz_id in valid_classes:
            values = self.testrun.test.get_raw_baseline_counter_values_for_class_id(clazz_id)
            base_median, base_stddev = self.baselines[clazz_id]
            num_outliers = int(len(values) * self.percent / 100)

            while num_outliers > 0:
                distance_from_first = math.fabs(base_median - values[0])
                distance_from_last = math.fabs(base_median - values[-1])
                if distance_from_first > distance_from_last:
                    del (values[0])
                else:
                    del (values[-1])
                num_outliers -= 1
            self.boundaries[clazz_id] = (values[0], values[-1])

    def get_category_boundaries(self, clazz_id):
        return list(self.boundaries[clazz_id]) if clazz_id in self.boundaries else []

    def get_outlier_boundaries(self, clazz_id):
        return self.boundaries[clazz_id]

    def classify(self, records, *args, **kwargs):
        """
        @param [ConsolidatedCounterRecord] records: records to classify
        @return [ConsolidatedCounterRecord]: classified records
        """
        for record in records:
            lower_bound, upper_bound = self.get_outlier_boundaries(record.clazz_id)

            if record.value < lower_bound:
                record.category = 'Low'
            elif record.value > upper_bound:
                record.category = 'High'
            else:
                record.category = 'Medium'
        return records
