import csv
import datetime
import logging
import traceback
import sys
import json

from django import forms
from django.core.urlresolvers import reverse, reverse_lazy
from django.db import IntegrityError
from django.http import HttpResponseForbidden, HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import ListView, DetailView, View, FormView, DeleteView, TemplateView
from livesettings import config_value
from pytz import utc

from repo import views_utils, models_utils
from repo.chart import repo_chart_timeseries
from repo.converters.csv_converter import CsvConverter
from repo.converters.jmeter_xml_converter import XmlConverter
from repo.forms import TestFileUploadForm, FileFormatChooserForm, CsvFileProcessorForm
from repo.models import Environment, Node, Test, TestRun, RawCounterRecord, ConsolidatedCounterRecord, \
    TestFile, Counter, CounterRecordClass
from repo.counter_record_generator import create_consolidated_counter_records
from repo.models_utils import invalidate_summary


class EnvironmentList(ListView):
    model = Environment

    def get_context_data(self, **kwargs):
        context = super(EnvironmentList, self).get_context_data(**kwargs)
        context[u'title'] = _('Environments')
        return context


class EnvironmentDetail(DetailView):
    model = Environment

    def get_context_data(self, **kwargs):
        context = super(EnvironmentDetail, self).get_context_data(**kwargs)
        context[u'title'] = _('%(environment)s environment') % {'environment': self.get_object().name}
        return context


class CounterList(ListView):
    model = Counter

    def get_context_data(self, **kwargs):
        context = super(CounterList, self).get_context_data(**kwargs)
        context[u'title'] = _('Counters')
        return context


class CounterDetail(DetailView):
    model = Counter

    def get_context_data(self, **kwargs):
        context = super(CounterDetail, self).get_context_data(**kwargs)
        context[u'title'] = _('%(counter)s counter') % {'counter': self.get_object().name}
        return context


class NodeList(ListView):
    global kwargs
    model = Node

    def get_context_data(self, **kwargs):
        context = super(NodeList, self).get_context_data(**kwargs)
        environment = Environment.objects.get(pk=self.kwargs['environment'])
        context[u'title'] = _('Nodes at %(environment)s') % {'environment': environment, }
        return context

    def get_queryset(self):
        self.environment = get_object_or_404(Environment, id=self.kwargs['environment'])
        return Node.objects.filter(environment=self.environment)


class NodeDetail(DetailView):
    global kwargs
    model = Node

    def get_context_data(self, **kwargs):
        context = super(NodeDetail, self).get_context_data(**kwargs)
        context[u'title'] = _('%(node)s node at %(environment)s') \
            % {'node': self.get_object().name, 'environment': self.get_object().environment.name}
        return context

    def get_object(self, queryset=None):
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        return get_object_or_404(Node, pk=self.kwargs['pk'], environment=self.environment)


class NodeDelete(DeleteView):
    global kwargs
    model = Node

    def get_success_url(self):
        return reverse_lazy('repo_environment_details', args=[self.kwargs['environment']])


class TestList(ListView):
    global kwargs
    model = Test

    def get_context_data(self, **kwargs):
        context = super(TestList, self).get_context_data(**kwargs)
        self.environment = Environment.objects.get(pk=self.kwargs['environment'])
        context[u'title'] = _('Tests at %(environment)s') % {'environment': self.environment}
        return context

    def get_queryset(self):
        self.environment = get_object_or_404(Environment, id=self.kwargs['environment'])
        return Test.objects.filter(environment=self.environment)


class TestDetail(DetailView):
    global kwargs
    model = Test

    def get_context_data(self, **kwargs):
        baseline_ids = self.get_object().get_baseline_testrun_ids(limit=True)
        context = super(TestDetail, self).get_context_data(**kwargs)
        context[u'title'] = self.get_object().__unicode__()
        context[u'raw_summary'] = models_utils.get_baseline_summary(baseline_ids, raw=True)
        context[u'disabled_class_ids'] = [clazz.id for clazz in self.get_object().disabled_counters.all()]
        return context

    def get_object(self, queryset=None):
        """
        @return Test: test
        """
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        return get_object_or_404(Test, pk=self.kwargs['pk'], environment=self.environment)


class TestRunList(ListView):
    global kwargs
    model = TestRun

    def get_context_data(self, **kwargs):
        context = super(TestRunList, self).get_context_data(**kwargs)
        self.test = Test.objects.get(pk=self.kwargs['test']).name
        context[u'title'] = _('Test runs for %(test)s') % {'test': self.test}
        return context

    def get_queryset(self):
        self.environment = get_object_or_404(Environment, id=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment.id)
        return TestRun.objects.filter(test=self.test)


class TestRunDisplay(DetailView):
    global kwargs
    model = TestRun

    def get_context_data(self, **kwargs):
        baseline_ids = self.get_object().test.get_baseline_testrun_ids(limit=True)
        raw_summary = views_utils.prepare_summary(
            models_utils.get_summary_for_testrun(self.get_object().id, baseline_run_ids=baseline_ids, raw=True))

        context = {'form': TestFileUploadForm(),
                   'title': self.get_object().__unicode__(),
                   'raw_summary': raw_summary,
                   'disabled_class_ids': [clazz.id for clazz in self.get_object().test.disabled_counters.all()]}
        context.update(kwargs)
        return super(TestRunDisplay, self).get_context_data(**context)

    def get_object(self, queryset=None):
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        return get_object_or_404(TestRun, pk=self.kwargs['pk'], test=self.test)


class TestRunDetail(View):
    def get(self, request, *args, **kwargs):
        view = TestRunDisplay.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = TestFileAdd.as_view()
        return view(request, *args, **kwargs)


class TestFileList(ListView):
    global kwargs
    model = TestRunList

    def get_context_data(self, **kwargs):
        context = super(TestFileList, self).get_context_data(**kwargs)
        self.testrun = TestRun.objects.get(pk=self.kwargs['testrun']).name
        context[u'title'] = _('Test files for run %(testrun)s') % {'testrun': self.testrun}
        return context

    def get_queryset(self):
        self.environment = get_object_or_404(Environment, id=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment.id)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        return TestFile.objects.filter(testrun=self.testrun)


class TestFileDetail(DetailView):
    global kwargs
    model = TestFile

    def get_context_data(self, **kwargs):
        context = super(TestFileDetail, self).get_context_data(**kwargs)
        context[u'title'] = _('Test file details')
        return context

    def get_object(self, queryset=None):
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        return get_object_or_404(TestFile, pk=self.kwargs['pk'], testrun=self.testrun)


class TestFileAdd(FormView):
    global kwargs
    global request
    template_name = 'repo/testrun_detail.html'
    form_class = TestFileUploadForm
    model = TestFile

    def get_redirect_url(self, file_id):
        return reverse('repo_test_file_process', args=[self.kwargs['environment'],
                                                       self.kwargs['test'],
                                                       self.kwargs['pk'],
                                                       file_id])

    def form_valid(self, form):
        #TODO this validation is not working
        if not self.request.user.is_authenticated():
            return HttpResponseForbidden

        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['pk'], test=self.test)
        new_file = TestFile(testrun=self.testrun, file=self.request.FILES['file'])
        new_file.original_name = self.request.FILES['file']._name
        new_file.uploaded_at = datetime.datetime.utcnow().replace(tzinfo=utc)
        new_file.save()

        #This return is lousy, should be
        #return super(TestFileAdd,self).form_valid(form)
        #with get_success_url instead of get_redirect_url,
        #but I don't know how to pass new_file.id to get_success_url correctly.
        #Loser me.
        return HttpResponseRedirect(self.get_redirect_url(new_file.id))


class EnvironmentDelete(DeleteView):
    global kwargs
    model = Environment
    template_name = 'repo/environment_list.html'

    def delete(self, request, *args, **kwargs):
        environment = get_object_or_404(Environment, pk=self.kwargs['pk'])
        files = environment.test_files()
        for f in files:
            f.delete_all_records()
        return super(EnvironmentDelete, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('repo_environment_list', args=[self.kwargs['pk']])


class CounterDelete(DeleteView):
    global kwargs
    model = Counter
    template_name = 'repo/counter_list.html'

    def get_success_url(self):
        return reverse_lazy('repo_counters')


class TestDelete(DeleteView):
    global kwargs
    model = Test
    template_name = 'repo/environment_detail.html'

    def delete(self, request, *args, **kwargs):
        test = get_object_or_404(Test, environment=self.kwargs['environment'],
                                 pk=self.kwargs['pk'])
        files = test.test_files()
        for f in files:
            f.delete_all_records()
        return super(TestDelete, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('repo_environment_details', args=[self.kwargs['environment']])


class TestRunDelete(DeleteView):
    global kwargs
    model = TestRun
    template_name = 'repo/test_detail.html'

    def delete(self, request, *args, **kwargs):
        testrun = get_object_or_404(TestRun, test=self.kwargs['test'],
                                    pk=self.kwargs['pk'])
        files = testrun.testfile_set.all()
        for f in files:
            f.delete_all_records()
        return super(TestRunDelete, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('repo_test_details', args=[self.kwargs['environment'], self.kwargs['test']])


class TestFileDelete(DeleteView):
    global kwargs
    model = TestFile
    template_name = 'repo/testrun_detail.html'

    def delete(self, request, *args, **kwargs):
        f = get_object_or_404(TestFile, pk=self.kwargs['pk'])
        f.delete_all_records()
        baseline_ids = f.testrun.test.get_baseline_testrun_ids()
        invalidate_summary(f.testrun.id, baseline_ids)
        return super(TestFileDelete, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('repo_testrun_details',
                            args=[self.kwargs['environment'], self.kwargs['test'], self.kwargs['testrun']])


class TestFileFormatChooser(FormView):
    global kwargs
    template_name = 'repo/test_file_process.html'
    form_class = FileFormatChooserForm

    def get_context_data(self, **kwargs):
        context = super(TestFileFormatChooser, self).get_context_data(**kwargs)
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.test)
        self.test_file = get_object_or_404(TestFile, pk=self.kwargs['pk'], testrun=self.testrun)
        context[u'title'] = _('Processing test file %(file)s') % {'file': self.test_file.original_name}
        context[u'test_file'] = self.test_file
        return context

    def form_valid(self, form):
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.test)
        self.test_file = get_object_or_404(TestFile, pk=self.kwargs['pk'], testrun=self.testrun)
        split = form.cleaned_data['format'].split('.')
        extension, file_format = split[0], split[1]
        return HttpResponseRedirect(reverse('repo_test_file_process', args=[str(self.environment.id),
                                                                            str(self.test.id),
                                                                            str(self.testrun.id),
                                                                            str(self.test_file.id), ])
                                    + extension + '/' + file_format + '/')


#A generic class to store common file processing methods
class TestFileProcessGeneric(TemplateView):
    global kwargs
    logger = logging.getLogger(__name__)

    def get_file(self):
        """
        @return TestFile: test file
        """
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.test)
        self.test_file = get_object_or_404(TestFile, pk=self.kwargs['pk'], testrun=self.testrun)
        return self.test_file

    def get_params(self):
        """
        Is redefined in child classes, return parameters for get() method.
        """
        return None

    def process_data(self, params, converter):
        f = self.get_file()
        f.delete_all_records()
        baseline_ids = f.testrun.test.get_baseline_testrun_ids()
        invalidate_summary(f.testrun.id, baseline_ids)
        try:
            converter.convert_to_raw_counter_records(params)
            create_consolidated_counter_records(f.testrun, f)
        except IntegrityError:
            message = traceback.format_exception_only(sys.exc_type, sys.exc_value)[0]
            self.logger.error(message)
            return HttpResponseRedirect(
                reverse('repo_test_file_process_error', args=[self.kwargs['environment'],
                                                              self.kwargs['test'],
                                                              self.kwargs['testrun'],
                                                              self.kwargs['pk'], ]))

        return HttpResponseRedirect(
            reverse('repo_testrun_details', args=[self.kwargs['environment'],
                                                  self.kwargs['test'],
                                                  self.kwargs['testrun']]))


class TestFileProcessCsv(TestFileProcessGeneric):
    global kwargs
    template_name = 'repo/test_file_process_csv_generic.html'
    form_class = CsvFileProcessorForm

    def get_columns(self):
        """
        Return a tuple of tuples (column,column), where the columns are the headers of CSV file
        if the file has headers or auto-generated column titles otherwise
        """
        f = self.get_file().file._get_path()

        #Parsing csv file to correctly get list of headers
        with open(f, 'rb') as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(4096))
            csvfile.seek(0)
            reader = csv.reader(csvfile, dialect)
            if csv.Sniffer().has_header(csvfile.read(4096)):
                csvfile.seek(0)
                headers = sorted(reader.next())
            else:
                csvfile.seek(0)
                headers_length = len(reader.next())
                headers = ['Column ' + str(i) for i in range(1, headers_length)]

        return tuple([(i, i) for i in headers])

    def get_context_data(self, **kwargs):
        context = super(TestFileProcessCsv, self).get_context_data(**kwargs)
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.test)
        self.test_file = get_object_or_404(TestFile, pk=self.kwargs['pk'], testrun=self.testrun)
        context['title'] = _('Processing .csv file %(file)s') % {'file': self.test_file.original_name}
        context['test_file'] = self.test_file
        context['format'] = self.kwargs['format']
        if self.kwargs['format'] == 'generic_node':
            context['from_node'] = True
        else:
            context['from_node'] = False
        return context

    def form_valid(self, form):
        f = self.get_file()
        params = form.get_conversion_parameters(f, self.kwargs['testrun'])
        return self.process_data(params, CsvConverter())

    def get(self, request, *args, **kwargs):
        column_choices = self.get_columns()
        if self.kwargs['format'] == 'generic_node':
            form = CsvFileProcessorForm(columns=column_choices, from_node=True, env_id=self.kwargs['environment'])
        else:
            form = CsvFileProcessorForm(columns=column_choices, from_node=False, env_id=self.kwargs['environment'])
        return self.render_to_response(self.get_context_data(form=form))

    #noinspection PyUnusedLocal
    def post(self, request, *args, **kwargs):
        column_choices = self.get_columns()
        if self.kwargs['format'] == 'generic_node':
            form = CsvFileProcessorForm(column_choices, True, self.kwargs['environment'], request.POST)
        else:
            form = CsvFileProcessorForm(column_choices, False, self.kwargs['environment'], request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form))


class TestFileProcessCsvJmeter(TestFileProcessGeneric):
    global kwargs

    def get_params(self):
        return self.get_file().get_parameters_for_jmeter_csv()

    def get(self, request, *args, **kwargs):
        return self.process_data(self.get_params(), CsvConverter())


class TestFileProcessCsvOpenTSDB(TestFileProcessGeneric):
    global kwargs

    def get_params(self):
        return self.get_file().get_parameters_for_opentsdb()

    def get(self, request, *args, **kwargs):
        return self.process_data(self.get_params(), CsvConverter())


class TestFileProcessCsvOpenTSDBDell(TestFileProcessGeneric):
    global kwargs

    def get_params(self):
        return self.get_file().get_parameters_for_opentsdb_dell()

    def get(self, request, *args, **kwargs):
        return self.process_data(self.get_params(), CsvConverter())


class TestFileProcessXmlJmeter(TestFileProcessGeneric):
    global kwargs

    def get_params(self):
        return self.get_file().get_parameters_for_jmeter_xml()

    def get(self, request, *args, **kwargs):
        return self.process_data(self.get_params(), XmlConverter())


class RawCounterRecordList(ListView):
    global kwargs
    model = RawCounterRecord
    paginate_by = config_value('repo_view', 'PAGINATE_BY')

    def get_context_data(self, **kwargs):
        context = super(RawCounterRecordList, self).get_context_data(**kwargs)
        self.testrun = TestRun.objects.get(pk=self.kwargs['testrun'])
        context['title'] = _('Raw counter records for test run %(testrun)s') % {'testrun': self.testrun.name}
        context['sidebar_title'] = _('Actions')
        context['testrun'] = self.testrun
        return context

    def get_queryset(self):
        self.environment = get_object_or_404(Environment, id=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment.id)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        return RawCounterRecord.objects.filter(testrun=self.testrun).select_related('clazz__label',
                                                                                    'clazz__counter__name')


class RawCounterRecordDeleteAll(FormView):
    global kwargs
    form_class = forms.Form

    def get_success_url(self):
        return reverse_lazy('repo_raw_counter_records',
                            args=[self.kwargs['environment'], self.kwargs['test'], self.kwargs['testrun']])

    def form_valid(self, form):
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        self.testrun.remove_raw_counter_records()
        return HttpResponseRedirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        raise Http404


class RawCounterRecordDetail(DetailView):
    global kwargs
    model = RawCounterRecord

    def get_context_data(self, **kwargs):
        context = super(RawCounterRecordDetail, self).get_context_data(**kwargs)
        context[u'title'] = _('Raw counter records details')
        return context

    def get_object(self, queryset=None):
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        return get_object_or_404(RawCounterRecord, pk=self.kwargs['pk'], testrun=self.testrun)


class CounterRecordList(ListView):
    global kwargs
    model = ConsolidatedCounterRecord
    paginate_by = config_value('repo_view', 'PAGINATE_BY')

    def get_context_data(self, **kwargs):
        context = super(CounterRecordList, self).get_context_data(**kwargs)
        self.testrun = TestRun.objects.get(pk=self.kwargs['testrun'])
        context['title'] = _('Consolidated counter records for test run %(testrun)s') % {'testrun': self.testrun.name}
        context['sidebar_title'] = _('Actions')
        context['testrun'] = self.testrun
        return context

    def get_queryset(self):
        self.environment = get_object_or_404(Environment, id=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment.id)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        return ConsolidatedCounterRecord.objects.filter(testrun=self.testrun).select_related('clazz__label',
                                                                                             'clazz__counter__name')


class CounterRecordDeleteAll(FormView):
    global kwargs
    form_class = forms.Form

    def get_success_url(self):
        return reverse_lazy('repo_consolidated_counter_records',
                            args=[self.kwargs['environment'], self.kwargs['test'], self.kwargs['testrun']])

    def form_valid(self, form):
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        self.testrun.remove_consolidated_counter_records()
        return HttpResponseRedirect(self.get_success_url())


class CounterRecordDetail(DetailView):
    global kwargs
    model = ConsolidatedCounterRecord

    def get_context_data(self, **kwargs):
        context = super(CounterRecordDetail, self).get_context_data(**kwargs)
        context[u'title'] = _('Consolidated counter record details')
        return context

    def get_object(self, queryset=None):
        self.environment = get_object_or_404(Environment, pk=self.kwargs['environment'])
        self.test = get_object_or_404(Test, pk=self.kwargs['test'], environment=self.environment)
        self.testrun = get_object_or_404(TestRun, pk=self.kwargs['testrun'], test=self.kwargs['test'])
        return get_object_or_404(ConsolidatedCounterRecord, pk=self.kwargs['pk'], testrun=self.testrun)


#noinspection PyUnusedLocal
def repo_chart_timeseries_raw(request, environment, test, testrun, counter):
    """
    Generate time-series chart for raw data.
    @param HttpRequest request: request
    @param int environment: environment id
    @param int test: Test id
    @param int testrun: Testrun id
    @param int counter: CounterRecordClass id
    @return HttpResponse response
    """
    response = HttpResponse(content_type='image/png')
    chart = repo_chart_timeseries(int(test), int(testrun), int(counter), True)
    chart.print_png(response)
    return response


#noinspection PyUnusedLocal
def repo_chart_timeseries_discrete(request, environment, test, testrun, counter):
    """
    Generate time-series chart for discrete data.
    @param HttpRequest request: request
    @param int environment: environment id
    @param int test: Test id
    @param int testrun: TestRun id
    @param int counter: CounterRecordClass id
    @return HttpResponse response
    """
    response = HttpResponse(content_type='image/png')
    chart = repo_chart_timeseries(int(test), int(testrun), int(counter), False)
    chart.print_png(response)
    return response


def ajax_change_counter_class_status(request):
    if not request.POST:
        response = HttpResponse(content='HTTP method %s is not supported by this URL' % request.method, status=405)
        response.__setitem__('Allow', 'POST')
        return response

    try:
        test_id = request.POST.get('test_id')
        class_id = request.POST.get('class_id')
    except KeyError:
        return HttpResponse(status=400)

    counter_class = CounterRecordClass.objects.get(pk=class_id)
    test = Test.objects.get(pk=test_id)
    if counter_class in CounterRecordClass.objects.filter(test__id__exact=test_id):
        test.disabled_counters.remove(counter_class)
    else:
        test.disabled_counters.add(counter_class)
    test.save()
    return HttpResponse(json.dumps(get_disabled_counter_classes(test)), mimetype='application/json')


def get_disabled_counter_classes(test):
    response_dict = dict()
    response_dict['test_id'] = test.id
    response_dict['disabled_counter_classes'] = [clazz.id for clazz in test.disabled_counters.all()]
    response_dict['success'] = True
    return response_dict