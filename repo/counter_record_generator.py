"""
Build consolidated counter records out of raw counter records and store them in a database.
"""
import datetime
from livesettings import config_value
import math
import numpy
from repo.models import RawCounterRecord, ConsolidatedCounterRecord


def create_consolidated_counter_records(testrun, f):
    classes = testrun.get_valid_classes()
    records = []
    for class_id in classes:
        #All raw records with given label and counter for the test run
        raw_records = list(RawCounterRecord.objects.select_related('clazz').
            filter(clazz__id=class_id, testrun__id=testrun.id, file__id=f.id).
            order_by('timestamp'))
        period_start = testrun.time_start
        offset = 0
        while period_start < testrun.time_end and raw_records:
            record = ConsolidatedCounterRecord()
            record.clazz = raw_records[0].clazz
            record.testrun = testrun
            record.offset = offset
            record.file = f
            values_in_period = []
            current_timestamp = raw_records[0].timestamp
            while raw_records and \
                    current_timestamp <= period_start + datetime.timedelta(0, testrun.test.sampling_period):

                raw_record = raw_records.pop(0)
                values_in_period.append(raw_record.value)
                if raw_records:
                    current_timestamp = raw_records[0].timestamp

            record.value = numpy.median(values_in_period)

            #TODO: issue 30: extrapolation instead of skipping empty records
            if record.value and not math.isnan(record.value):
                records.append(record)
            if len(records) % config_value('repo_model', 'BATCH_INSERT_SIZE') == 0:
                ConsolidatedCounterRecord.objects.bulk_create(records)
                records = []

            period_start += datetime.timedelta(0, testrun.test.sampling_period)
            offset += 1

    ConsolidatedCounterRecord.objects.bulk_create(records)