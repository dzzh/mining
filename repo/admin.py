from django.contrib import admin
from repo.models import Environment, Node, TestRun, Test, CounterRecordClass
from repo.models import ConsolidatedCounterRecord, RawCounterRecord, TestFile, Counter

#Registering the models
admin.site.register(Environment)
admin.site.register(Node)
admin.site.register(Test)
admin.site.register(TestRun)
admin.site.register(TestFile)
admin.site.register(Counter)
admin.site.register(CounterRecordClass)
admin.site.register(ConsolidatedCounterRecord)
admin.site.register(RawCounterRecord)