from django import forms
from django.utils.translation import ugettext as _
from repo import ConverterParametersCsv
from repo.models import Counter, Node, TestRun


class TestFileUploadForm(forms.Form):
    file = forms.FileField()


class FileFormatChooserForm(forms.Form):
    format = forms.ChoiceField(choices=(
        ('csv.jmeter', 'Apache JMeter .csv'),
        ('xml.jmeter', 'Apache JMeter .jtl'),
        ('csv.opentsdb', 'OpenTSDB .csv CPU/MySQL'),
        ('csv.opentsdb_dell', 'OpenTSDB .csv for Dell case study'),
        ('csv.generic_env', 'Generic environment .csv'),
        ('csv.generic_node', 'Generic node .csv'),
    ), label=_('File format:'), )


class CsvFileProcessorForm(forms.Form):
    global fields
    global data
    counters_count = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, columns=None, from_node=False, env_id=None, *args, **kwargs):
        super(CsvFileProcessorForm, self).__init__(*args, **kwargs)
        self.from_node = from_node
        count = int(self.data.get('counters_count', 0))
        counters = [(counter.id, counter.name) for counter in Counter.objects.all().order_by('name')]
        self.fields['counters_count'].initial = count
        self.fields['timestamp_column'] = forms.ChoiceField(label=_('Timestamp header: '), choices=columns)
        if from_node:
            nodes_list = Node.objects.filter(environment__id=env_id)
            nodes_column = []
            for node in nodes_list:
                nodes_column.append((node.id, node.name))
            self.fields['node'] = forms.ChoiceField(label=_('Computation node: '), choices=nodes_column)
        else:
            self.fields['label_column'] = forms.ChoiceField(label=_('Scenario label header: '), choices=columns)
        self.fields['counter_1'] = forms.ChoiceField(label=_('Counter %(index)d: ' % {'index': 1}),
                                                     choices=tuple(counters))
        self.fields['column_1'] = forms.ChoiceField(label=_('Header %(index)d: ' % {'index': 1}), choices=columns)

        #Dynamic creation of additional counter fields
        for index in range(count):
            self.fields['counter_{index}'.format(index=index + 2)] = \
                forms.ChoiceField(label=_('Counter %(index)d: ' % {'index': index + 2}), choices=tuple(counters))
            self.fields['column_{index}'.format(index=index + 2)] = \
                forms.ChoiceField(label=_('Header %(index)d: ' % {'index': index + 2}), choices=columns)

    def clean(self):
        cleaned_data = super(CsvFileProcessorForm, self).clean()
        errors = []
        error_messages = []

        #
        #Validate uniqueness of counters data
        #
        counters = []
        for field_name, field_value in self.fields.iteritems():
            if field_name.startswith('counter') and type(field_value) == forms.ChoiceField:
                if cleaned_data[field_name] in counters:
                    if not cleaned_data[field_name] in errors:
                        errors.append(cleaned_data[field_name])
                        counter = field_value.choices[int(cleaned_data[field_name]) - 1][1]
                        error_messages.append(
                            _('Counter <em>%(counter)s</em> is repeated more than once.' % {'counter': counter}))
                else:
                    counters.append(cleaned_data[field_name])

        #
        #Validate uniqueness of columns data
        #
        columns = []
        errors = []

        columns.append(cleaned_data['timestamp_column'])
        #Check that timestamp column does not equal to label column
        if not self.from_node:
            if cleaned_data['timestamp_column'] == cleaned_data['label_column'] and \
                    not cleaned_data['timestamp_column'] in errors:

                errors.append(cleaned_data['timestamp_column'])
                error_messages.append(_('Column header <em>%(column)s</em> is repeated more than once.' % {
                    'column': cleaned_data['timestamp_column']}))
            columns.append(cleaned_data['label_column'])

        #Then proceed with columns used for association with counters
        for field_name, field_value in self.fields.iteritems():
            if field_name.startswith('column') and type(field_value) == forms.ChoiceField:
                if cleaned_data[field_name] in columns:
                    if not cleaned_data[field_name] in errors:
                        errors.append(cleaned_data[field_name])
                        error_messages.append(_('Column header <em>%(column)s</em> is repeated more than once.' % {
                            'column': cleaned_data[field_name]}))
                else:
                    columns.append(cleaned_data[field_name])

        if error_messages:
            raise forms.ValidationError(error_messages)

        return cleaned_data

    def get_conversion_parameters(self, test_file, testrun_id):
        cleaned_data = self.clean()
        testrun = TestRun.objects.get(pk=testrun_id)

        parameters = ConverterParametersCsv(test_file)
        parameters.testrun_id = testrun_id
        parameters.testrun_start = testrun.time_start
        parameters.testrun_end = testrun.time_end
        parameters.node_related = self.from_node
        parameters.timestamp_column = cleaned_data['timestamp_column']

        if self.from_node:
            parameters.node_id = cleaned_data['node']
        else:
            parameters.label_column = 'label'

        for field_name in self.fields.keys():
            if field_name.startswith('counter_'):
                number = field_name.split('_')[1]
                counter_id = int(cleaned_data[field_name])
                column = cleaned_data['column_' + number]
                parameters.counters.append((counter_id, column))

        return parameters