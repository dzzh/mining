from django.conf.urls import patterns, url
from analysis.views import EnvironmentList, TestDetail, TestRunDetail

urlpatterns = patterns('',
    url(r'tests/(?P<test>\d+)/testruns/(?P<testrun>\d+)/(?P<counter>\d+)/distributionChart.png$',
        'analysis.views.analysis_barchart_distribution', {}, 'analysis_chart_distribution'),

    url(r'tests/(?P<test>\d+)/testruns/(?P<pk>\d+)/$', TestRunDetail.as_view(), {}, 'analysis_testrun_details'),

    url(r'tests/(?P<pk>\d+)/testruns/$', TestDetail.as_view(), {}, 'analysis_test_details_testruns'),
    url(r'tests/(?P<pk>\d+)/$', TestDetail.as_view(), {}, 'analysis_test_details'),

    url(r'tests/$', EnvironmentList.as_view(), {}, 'analysis_index_tests'),
    url(r'$', EnvironmentList.as_view(), {}, 'analysis_index'),
)
