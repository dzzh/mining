from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from django.utils.translation import ugettext as _

from analysis.chart import analysis_barchart
from analysis.assoc_rules.rules_analysis import analyze_data
from repo.models import Environment, Test, TestRun


class EnvironmentList(ListView):
    model = Environment
    template_name = 'analysis/environment_list.html'

    def get_context_data(self, **kwargs):
        context = super(EnvironmentList, self).get_context_data(**kwargs)
        context[u'title'] = _('Select a test for analysis')
        return context


class TestDetail(DetailView):
    global kwargs
    model = Test
    template_name = 'analysis/test_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TestDetail, self).get_context_data(**kwargs)
        context[u'title'] = self.get_object().__unicode__()
        return context

    def get_object(self, queryset=None):
        """
        @return Test: test
        """
        return get_object_or_404(Test, pk=self.kwargs['pk'])


class TestRunDetail(DetailView):
    global kwargs
    template_name = 'analysis/testrun_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TestRunDetail, self).get_context_data(**kwargs)
        testrun = self.get_object()
        test = testrun.test
        baseline_transactions = test.get_baseline_transactions()
        target_transactions = testrun.get_transactions()
        context[u'title'] = _('Test run analysis')
        context[u'analysis_results'] = analyze_data(testrun)
        context[u'num_baseline_transactions'] = len(baseline_transactions)
        context[u'num_target_transactions'] = len(target_transactions)
        context[u'test'] = test
        return context

    def get_object(self, queryset=None):
        """
        @return TestRun: test run
        """
        return get_object_or_404(TestRun.objects.select_related(), pk=self.kwargs['pk'], test__id=self.kwargs['test'])


def analysis_barchart_distribution(request, test, testrun, counter):
    """
    Generate bar chart to present categories distribution.
    @param HttpRequest request: request
    @param int testrun: TestRun id
    @return HttpResponse response
    """
    testrun_object = get_object_or_404(TestRun, pk=testrun, test_id=test)

    chart = analysis_barchart(testrun_object, int(counter))

    response = HttpResponse(content_type='image/png')
    chart.print_png(response)
    return response
