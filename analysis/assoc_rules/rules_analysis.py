import logging
import math
import datetime

from cache_utils.decorators import cached
from livesettings import config_value

from analysis import FREQUENT_ITEMSETS_GENERATOR
from analysis.assoc_rules.utils import get_item_index, get_minimum_support
from analysis.assoc_rules.comparison_classes import AssociationRule, Consequent, ViolatingCounter
from mining import settings
from repo.models import CounterRecordClass

#Threshold for comparing dangers between two rules
FLOAT_THRESHOLD = 0.01

logger = logging.getLogger(__name__)


def analyze_data(testrun):
    """
    Main data analysis method. Retrieves frequent itemsets for baseline and target, compares
    the resulting support trees and outputs a data structure containing information
    about the differences between the datasets.
    @param TestRun testrun: target test run to analyze
    @return [ViolatingCounter]: list of violations
    """
    minsup_percent = config_value('analysis', 'MINSUP_PERCENTS')
    mindiff_percent = config_value('analysis', 'MINDIFF_PERCENTS')
    max_itemset_length = config_value('analysis', 'MAX_ITEMSET_LENGTH')
    return _analyze_data(testrun, minsup_percent, mindiff_percent, max_itemset_length)


#@cached(settings.CACHE_TIMEOUT)
#See https://bitbucket.org/kmike/django-cache-utils/issue/5/not-detecting-methods,
#needed to fix cache_utils when installing to new machine
def _analyze_data(testrun, minsup_percent, mindiff, max_itemset_length):
    """
    Main data analysis method. Retrieves frequent itemsets for baseline and target, compares
    the resulting support trees and outputs a data structure containing information
    about the differences between the datasets. Three int parameters are sent to function,
    not retrieved in it because of caching.
    (If user changes analysis-related settings between runs, the new call does not get cached.)
    @param TestRun testrun: target test run to analyze
    @param int minsup_percent: minimum support in percents
    @param int mindiff: minimum difference in percents
    @param int max_itemset_length: maximal length of an itemset to generate
    @return [ViolatingCounter]: list of violations
    """
    logger.info('Started analysis')
    t1 = datetime.datetime.now()
    baseline_transactions = testrun.test.get_baseline_transactions()
    target_transactions = testrun.get_transactions()

    #Calculate minsup and update minsup for target to better work with boundary situations where baseline
    #exceeds boundary a little, and target does not. If not decreasing target support,
    #then for such cases target will be always reported as 0.
    minsup = get_minimum_support(len(baseline_transactions), minsup_percent)
    target_minsup = get_minimum_support(len(target_transactions), minsup_percent)
    target_minsup -= int(target_minsup * mindiff / float(100))
    if target_minsup == minsup and target_minsup > 1:
        target_minsup -= 1
    if target_minsup < 1:
        target_minsup = 1

    generator = FREQUENT_ITEMSETS_GENERATOR()

    hashed_baseline = generator.hash_transactions(baseline_transactions)
    hashed_target = generator.hash_transactions(target_transactions)

    t2 = datetime.datetime.now()
    baseline_fp_tree = generator.build_fp_tree(hashed_baseline)
    target_fp_tree = generator.build_fp_tree(hashed_target)
    t3 = datetime.datetime.now()
    logger.info('Generated fp-trees in %s' % (t3 - t2))

    baseline_sup_tree = generator.find_frequent_itemsets(baseline_fp_tree, minsup, max_itemset_length)
    t4 = datetime.datetime.now()
    logger.info('Found itemsets with support >=%d in baseline in %s' % (minsup, t4 - t3))
    target_sup_tree = generator.find_frequent_itemsets(target_fp_tree, target_minsup, max_itemset_length)
    t5 = datetime.datetime.now()
    logger.info('Found itemsets with support >=%d in target in %s' % (target_minsup, t5 - t4))

    rules = compare_itemset_trees(baseline_sup_tree, baseline_fp_tree, target_sup_tree, target_fp_tree, mindiff)
    t6 = datetime.datetime.now()
    logger.info('Compared trees in %s' % (t6 - t5))
    result = optimize_rules(baseline_fp_tree, target_fp_tree, rules, minsup, mindiff)
    t7 = datetime.datetime.now()
    logger.info('Analysis completed in %s' % (t7 - t1))
    return result


def compare_itemset_trees(baseline_support_tree, baseline_fp_tree,
                          target_support_tree, target_fp_tree, mindiff):
    """
    Compare support trees and return a list of important association rules grouped by antecedents.
    @param SupportTree baseline_support_tree: support tree of baseline itemsets
    @param FPTree baseline_fp_tree: FP-tree built from baseline dataset
    @param SupportTree target_support_tree: support tree of target itemsets
    @param FPTree target_fp_tree: FP-tree built from target dataset
    @param int mindiff: minimum difference between supports in percents to consider violation important
    @return [AssociationRule]: list of violating association rules grouped by antecedents
    """
    scaling_factor = baseline_support_tree.root.support / float(target_support_tree.root.support)

    similar_itemsets = 0
    store_calls = [0]  # counter is implemented as a list to give nested function writing access to it (ugly, huh?)
    rules_created = [0]

    def store_rule(antecedent, consequent):
        """
        Store association rule in a list.
        @param [] antecedent: an itemset representing rule antecedent
        @param consequent: an item that is a consequent in a rule
        """
        store_calls[0] += 1

        #Calculate support for antecedents and consequents for both baseline and target
        if not antecedent:
            baseline_antecedent_support = baseline_fp_tree.num_transactions
        else:
            baseline_antecedent_node = baseline_support_tree.get_node_for_pattern(antecedent)
            baseline_antecedent_support = baseline_antecedent_node.support if baseline_antecedent_node \
                else baseline_fp_tree.get_itemset_support(antecedent)

        baseline_consequent_support = baseline_support_tree.get_node_for_pattern(antecedent + [consequent]).support

        if not antecedent:
            target_antecedent_support = target_fp_tree.num_transactions
        else:
            target_antecedent_node = target_support_tree.get_node_for_pattern(antecedent)
            target_antecedent_support = target_antecedent_node.support if target_antecedent_node \
                else target_fp_tree.get_itemset_support(antecedent)

        target_node = target_support_tree.get_node_for_pattern(antecedent + [consequent])
        target_consequent_support = target_node.support if target_node else 0

        #No need to store rules with dissimilar antecedents - they were stored with similar antecedents earlier
        if not are_itemset_supports_close(baseline_antecedent_support, target_antecedent_support * scaling_factor,
                                          mindiff):
            return

        rule = None
        is_rule_missing = False
        for r in reversed(rules):
            if r.antecedent == antecedent:
                rule = r
                break
        if not rule:
            rule = AssociationRule(antecedent, baseline_antecedent_support, target_antecedent_support, scaling_factor,
                                   baseline_fp_tree.num_transactions, target_fp_tree.num_transactions)
            is_rule_missing = True

        new_consequent = Consequent(consequent, baseline_consequent_support, target_consequent_support)

        rule.add_consequent(new_consequent)
        if is_rule_missing:
            rules.append(rule)
            rules_created[0] += 1

    def store_rules(baseline_node):
        """
        Generate and store association rules for a violation found between baseline and target trees.
        @param SupportNode baseline_node: node in baseline tree that hasn't corresponding non-violating node in target.
        """
        pattern = baseline_node.pattern()
        target_node = target_support_tree.get_node_for_pattern(pattern)
        target_support = target_node.support if target_node else 0

        if not target_support:
            #There is no corresponding node in the target tree, thus only one rule has to be generated
            antecedent = list(pattern)
            consequent = antecedent.pop()
            store_rule(antecedent, consequent)
        else:
            #Target tree has a corresponding node, all possible rules with 1-node consequent have to be checked
            for i in range(len(pattern)):
                antecedent = list(pattern)
                consequent = antecedent.pop(i)
                store_rule(antecedent, consequent)

    queue = []
    [queue.append(child) for _, child in baseline_support_tree.root.children]
    rules = []

    while queue:
        current_node = queue.pop(0)
        pattern = current_node.pattern()
        baseline_support = current_node.support
        target_node = target_support_tree.get_node_for_pattern(pattern)

        if target_node:
            target_support = target_node.support * scaling_factor
            if are_itemset_supports_close(baseline_support, target_support, mindiff):
                [queue.append(child) for _, child in current_node.children]
                similar_itemsets += 1
                logger.debug('+++ %s (%f, %f)' % (pattern, baseline_support, target_support))
            else:
                logger.debug('--- %s: confidence violation - (%f, %f)' % (pattern, baseline_support, target_support))
                store_rules(current_node)
        else:
            logger.debug('--- %s (%f)' % (pattern, baseline_support))
            store_rules(current_node)

    logger.info('Found similar itemsets: %d' % similar_itemsets)
    logger.info('Calls to store_rule: %d' % store_calls[0])
    logger.info('Association rules created: %d' % rules_created[0])

    return rules


def optimize_rules(baseline_fp_tree, target_fp_tree, rules, minsup, mindiff):
    """
    Remove rules with duplicating consequents from `rules` and add categories distribution for the remaining ones.
    @param FPTree baseline_fp_tree: fp-tree built from baseline dataset
    @param FPTree target_fp_tree: fp-tree built from target dataset
    @param [AssociationRule] rules: list of association rules
    @return [ViolatingCounter]: list of the most representative association rules
    """
    scaling_factor = baseline_fp_tree.num_transactions / float(target_fp_tree.num_transactions)
    generator = FREQUENT_ITEMSETS_GENERATOR()

    result = []
    for index in get_violating_indices(rules):
        pattern = get_violating_rule_for_index(rules, index).antecedent
        baseline_antecedent_support, baseline_consequent_supports = get_categories_distribution(baseline_fp_tree,
                                                                                                pattern, index)
        target_antecedent_support, target_consequent_supports = get_categories_distribution(target_fp_tree, pattern,
                                                                                            index)
        counter = CounterRecordClass.objects.select_related().get(pk=index)
        violation = ViolatingCounter(counter, pattern, baseline_antecedent_support, target_antecedent_support,
                                     baseline_fp_tree.num_transactions, target_fp_tree.num_transactions,
                                     minsup, mindiff, scaling_factor)
        items = set()
        for i in baseline_consequent_supports:
            items.add(i)
        for i in target_consequent_supports:
            items.add(i)
        for i in items:
            baseline_support = baseline_consequent_supports[i] if i in baseline_consequent_supports else 0
            target_support = target_consequent_supports[i] if i in target_consequent_supports else 0
            _, category = generator.decode_item(i)
            entry = Consequent(i, baseline_support, target_support)
            entry.category = category
            violation.add_consequent(entry)
        result.append(violation)

    #order by decreasing danger
    result.sort(key=lambda x: x.danger(), reverse=True)
    return result


def are_itemset_supports_close(baseline_support, target_support, mindiff):
    """
    Return True if supports differ by less than mindiff, False otherwise.
    @param float baseline_support: support of baseline itemset
    @param float target_support: support of float itemset
    @param float mindiff: mindiff
    """
    sup_diff = math.fabs(baseline_support - target_support) / float(baseline_support) * 100
    if sup_diff > mindiff:
        return False
    return True


def get_violating_indices(rules):
    """
    Return a merged list of indices for all the nodes that differ between the datasets.
    @param [AssociationRule] rules: result of analyzing the itemsets
    @return [] children: indices of children that differ somewhere between the itemsets
    """
    children = set()
    for node in rules:
        for child in node.consequents:
            children.add(get_item_index(child.item))
    return list(children)


def get_violating_rule_for_index(rules, index):
    """
    Take an index of a violating counter and find the most important rule for it to report.
    Violation is chosen by danger, then by support.
    @param [ViolatingCounter] rules: difference between the datasets
    @param index: index of a violating counter
    @return Consequent: most important violation
    """
    selected_rule = None
    selected_danger = None
    for rule in rules:
        if rule.has_consequent_with_index(index):
            if not selected_rule:
                selected_rule = rule
                selected_danger = rule.danger(index)
            else:
                current_danger = rule.danger(index)
                if current_danger > selected_danger and current_danger - selected_danger >= FLOAT_THRESHOLD:
                    selected_rule = rule
                    selected_danger = current_danger
                #If dangers are almost equal, take baseline support as tie breaker
                elif math.fabs(current_danger - selected_danger) < FLOAT_THRESHOLD \
                    and rule.baseline_support > selected_rule.baseline_support \
                        and rule.baseline_support - selected_rule.baseline_support >= FLOAT_THRESHOLD:
                    selected_rule = rule
                    selected_danger = current_danger
    return selected_rule


def get_categories_distribution(fp_tree, pattern, index):
    """
    Take counter index, find all its categories and return distribution of their supports.
    Use fp tree to make calculations.
    @param FPTree fp_tree: Instance of FP-tree storing representing the interesting dataset.
    @param [] pattern: pattern that have to be presented in all transaction we use for calculations
    @param index: index of a violating counter
    """
    pattern_support = fp_tree.get_itemset_support(pattern)
    categories = fp_tree.categories(get_item_index, index)
    support_dict = {}
    for category in categories:
        itemset = fp_tree.sort_transaction(pattern + [category])
        support_dict[category] = fp_tree.get_itemset_support(itemset)
    return pattern_support, support_dict