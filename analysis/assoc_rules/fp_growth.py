"""
Frequent itemsets generator based on FP-growth algorithm.
The algorithm is implemented based on: `Han, Pei, Yin, Mao - Mining Frequent Patterns without Candidate Generation:
A Frequent-Pattern Tree Approach. Data Mining and Knowledge Discovery-2004.`

Based on Eric Naeseth's FP-growth algorithm implementation (https://github.com/enaeseth/python-fp-growth).
"""

from collections import defaultdict
from itertools import imap
import logging
from operator import itemgetter
from analysis.assoc_rules.base_generator import BaseFrequentItemsetsGenerator
from analysis.assoc_rules.fp_tree import FPTree, FPNode
from analysis.assoc_rules.support_tree import SupportTree


class FPGrowthGenerator(BaseFrequentItemsetsGenerator):
    logger = logging.getLogger(__name__)

    def find_frequent_itemsets(self, fp_tree, minimum_support, max_itemset_length):
        """
        Process FP-tree to return a support tree containing the frequent itemsets.
        @param FPTree fp_tree: FP-tree used as a source
        @param int max_itemset_length: maximal length of itemset to process
        @param int minimum_support: number of transactions to consider itemset frequent
        """

        def find_with_suffix(tree, suffix):
            for item, nodes in tree.items():
                support = sum(n.count for n in nodes)
                if support >= minimum_support and item not in suffix:
                    found_set = [item] + suffix
                    yield sorted(found_set), support

                    # Build a conditional tree and recursively search for frequent itemsets within it.
                    cond_tree = self.conditional_tree_from_paths(tree.prefix_paths(item))
                    for s in find_with_suffix(cond_tree, found_set):
                        yield s  # pass along the good news to our caller

        #Retrieve frequent itemsets and merge them to remove subsets and retain only the longest ones.
        result = SupportTree(fp_tree.num_transactions, fp_tree.sorted_items())
        num_itemsets_total = 0
        num_itemsets_processed = 0
        for record, support in find_with_suffix(fp_tree, []):
            if (max_itemset_length and len(record) <= max_itemset_length) or not max_itemset_length:
                result.add_transaction(record, support, False)
                num_itemsets_processed += 1
            num_itemsets_total += 1

        if max_itemset_length:
            self.logger.info('Generated %d frequent itemsets, %d of them will be processed further.'
                             % (num_itemsets_total, num_itemsets_processed))
        else:
            self.logger.info('Generated %d frequent itemsets.' % num_itemsets_total)

        return result

    def build_fp_tree(self, transactions):
        """
        Build an FP-tree out of source dataset. This FP-tree contains all items, appearing in the transactions,
        not only frequent ones. This is needed as this property is used later for analysis.
        @param [] transactions: list of transactions
        @return FPTree
        """
        items = defaultdict(lambda: 0)

        # Load the passed-in transactions and count the support that individual items have.
        for transaction in transactions:
            for item in transaction:
                items[item] += 1

        def fp_sort_items():
            """
            Sort list of items with frequencies following FP-tree order (frequency-desc, natural-asc)
            @param [(item,frequency)]: list of items
            @return sorted list without frequencies
            """
            natural_order = sorted(items.iteritems(), key=itemgetter(0))
            full_sort = sorted(natural_order, key=itemgetter(1), reverse=True)
            return [item[0] for item in full_sort]

        sorted_items = fp_sort_items()

        def preprocess_transaction(transaction):
            """
            Prepare the transaction for addition to the tree: select only frequent items and sort
            them in frequency decreasing order.
            """
            transaction = filter(lambda v: v in items, transaction)
            transaction.sort(key=lambda v: sorted_items.index(v))
            return transaction

        tree = FPTree()

        for transaction in imap(preprocess_transaction, transactions):
            tree.add_transaction(transaction)
        return tree

    def conditional_tree_from_paths(self, paths):
        """Build a conditional FP-tree from the given prefix paths."""
        tree = FPTree()
        condition_item = None
        items = set()

        # Import the nodes in the paths into the new tree. Only the counts of the
        # leaf notes matter; the remaining counts will be reconstructed from the
        # leaf counts.
        for path in paths:
            if condition_item is None:
                condition_item = path[-1].item

            point = tree.root
            for node in path:
                next_point = point.search(node.item)
                if not next_point:
                    # Add a new node to the tree.
                    items.add(node.item)
                    count = node.count if node.item == condition_item else 0
                    next_point = FPNode(tree, node.item, count)
                    point.add_child(next_point)
                    tree._update_route(next_point)
                point = next_point

        assert condition_item is not None

        # Calculate the counts of the non-leaf nodes.
        for path in tree.prefix_paths(condition_item):
            count = path[-1].count
            for node in reversed(path[:-1]):
                node._count += count

        return tree
