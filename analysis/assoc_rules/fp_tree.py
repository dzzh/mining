"""
FP-tree implementation used in FP-growth algorithm for mining frequent itemsets.
The algorithm is implemented based on: `Han, Pei, Yin, Mao - Mining Frequent Patterns without Candidate Generation:
    A Frequent-Pattern Tree Approach. Data Mining and Knowledge Discovery-2004.`

Based on Eric Naeseth's FP-growth algorithm implementation (https://github.com/enaeseth/python-fp-growth).
"""

from collections import namedtuple
from operator import itemgetter


class FPTree(object):
    """
    An FP tree.

    This object may only store transaction items that are hashable (i.e., all
    items must be valid as dictionary keys or set members).
    """

    Route = namedtuple('Route', 'head tail')

    def __init__(self):
        # The root node of the tree.
        self._root = FPNode(self, None, None)

        # A dictionary mapping items to the head and tail of a path of
        # "neighbors" that will hit every node containing that item.
        self._routes = {}

        #Number of transactions stored in the tree.
        self._num_transactions = 0

        #Cache of the list of FP-ordered items
        self._items_cache = []

    @property
    def root(self):
        return self._root

    @property
    def num_transactions(self):
        return self._num_transactions

    def add_transaction(self, transaction):
        """
        Process a transaction and store its items in the tree.
        @param [] transaction: transaction to process.
        """
        self._num_transactions += 1
        current_node = self._root
        self._items_cache = []  # tree changes, clear the cache to recalculate it later
        for item in transaction:
            next_node = current_node.search(item)
            if next_node:
                # There is already a node in this tree for the current transaction item; reuse it.
                next_node.increment_count()
            else:
                # Create a new current_node and add it as a child of the current_node we're currently looking at.
                next_node = FPNode(self, item)
                current_node.add_child(next_node)

                # Update the route of nodes that contain this item to include our new node.
                self._update_route(next_node)

            current_node = next_node

    def _update_route(self, node):
        """Add the given node to the route through all nodes for its item."""
        assert self is node.tree

        try:
            route = self._routes[node.item]
            route[1].neighbor = node  # route[1] is the tail
            self._routes[node.item] = self.Route(route[0], node)
        except KeyError:
            # First node for this item; start a new route.
            self._routes[node.item] = self.Route(node, node)

    def items(self):
        """
        Generate one 2-tuples for each item represented in the tree. The first
        element of the tuple is the item itself, and the second element is a
        generator that will yield the nodes in the tree that belong to the item.
        """
        for item in self._routes.iterkeys():
            yield (item, self.nodes(item))

    def frequencies(self):
        """
        Return dictionary with item frequencies.
        """
        result = {}
        for item, nodes in self.items():
            result[item] = sum([node.count for node in nodes])
        return result

    def sorted_items(self):
        """
        Return list item sorted in desc FP-tree order (first desc frequency, then asc natural).
        The result is sorted first by frequency in descending order, then by item in ascending order.
        After the first calculation the items are stored in cache and reused later if the tree hasn't changed.
        """
        if not self._items_cache:
            natural_order = sorted(self.frequencies().iteritems(), key=itemgetter(0))
            full_sort = sorted(natural_order, key=itemgetter(1), reverse=True)
            self._items_cache = [item[0] for item in full_sort]
        return self._items_cache

    def get_itemset_support(self, itemset):
        """
        Return support of a given itemset.
        @param [] itemset: itemset to use
        """
        if not itemset:
            return self.num_transactions

        sorted_itemset = self.sort_transaction(itemset)

        last_item = sorted_itemset[-1]
        paths = self.prefix_paths(last_item)
        support = 0
        for path in paths:
            itemset_copy = list(sorted_itemset)
            path_support = path[-1].count
            for node in path:
                if node.item in itemset_copy:
                    itemset_copy.remove(node.item)
            if not itemset_copy:
                support += path_support
        return support

    def sort_transaction(self, transaction):
        """
        Sort items in transaction in FP-order.
        @param [] transaction: transaction to sort.
        @return transaction: sorted transaction.
        """
        return sorted(transaction, key=lambda x: self.sorted_items().index(x))

    def nodes(self, item):
        """
        Generate the sequence of nodes that contain the given item.
        """
        try:
            node = self._routes[item][0]
        except KeyError:
            return

        while node:
            yield node
            node = node.neighbor

    def prefix_paths(self, item):
        """Generate the prefix paths that end with the given item."""

        def collect_path(node):
            path = []
            while node and not node.root:
                path.append(node)
                node = node.parent
            path.reverse()
            return path

        return (collect_path(node) for node in self.nodes(item))

    def categories(self, index_function, index):
        """Get all categories existing in the tree for a given index."""
        for item in self.items():
            category = item[0]
            if index_function(category) == index:
                yield category

    def inspect(self):
        print 'Tree:'
        self.root.inspect(1)

        print
        print 'Routes:'
        for item, nodes in self.items():
            print '  %r' % item
            for node in nodes:
                print '    %r' % node


class FPNode(object):
    """A node in an FP tree."""

    def __init__(self, tree, item, count=1):
        self._tree = tree
        self._item = item
        self._count = count
        self._parent = None
        self._children = {}
        self._neighbor = None
        self._level = 0

    def add_child(self, child):
        """Add the given FPNode `child` as a child of this node.
        @param FPNode child: child to add
        """
        if not child.item in self._children:
            self._children[child.item] = child
            child.parent = self
            child.level = self.level + 1

    def search(self, item):
        """If a current node has a child with a given item, return it. Return None otherwise."""
        if item in self._children:
            return self._children[item]
        else:
            return None

    def __contains__(self, item):
        return item in self._children

    @property
    def tree(self):
        """The tree in which this node appears."""
        return self._tree

    @property
    def item(self):
        """The item contained in this node."""
        return self._item

    @property
    def count(self):
        """The count associated with this node's item."""
        return self._count

    def increment_count(self):
        """Increment the count associated with this node's item."""
        if self._count is None:
            raise ValueError('Root nodes have no associated count.')
        self._count += 1

    @property
    def root(self):
        """True if this node is the root of a tree; false if otherwise."""
        return self._item is None and self._count is None

    @property
    def leaf(self):
        """True if this node is a leaf in the tree; false if otherwise."""
        return len(self._children) == 0

    @property
    def parent(self):
        """The node's parent."""
        return self._parent

    @parent.setter
    def parent(self, value):
        if value is not None and not isinstance(value, FPNode):
            raise TypeError('A node must have an FPNode as a parent.')
        if value and value.tree is not self.tree:
            raise ValueError("Cannot have a parent from another tree.")
        self._parent = value

    @property
    def neighbor(self):
        """The node's neighbor; the one with the same value that is "to the right of it in the tree."""
        return self._neighbor

    @neighbor.setter
    def neighbor(self, value):
        if value is not None and not isinstance(value, FPNode):
            raise TypeError("A node must have an FPNode as a neighbor.")
        if value and value.tree is not self.tree:
            raise ValueError("Cannot have a neighbor from another tree.")
        self._neighbor = value

    @property
    def level(self):
        """A level of node in the tree, 0 for root"""
        return self._level

    @level.setter
    def level(self, value):
        self._level = value

    @property
    def children(self):
        """The nodes that are children of this node."""
        return tuple(self._children.itervalues())

    def inspect(self, depth=0):
        print ('  ' * depth) + repr(self)
        for child in self.children:
            child.inspect(depth + 1)

    def __repr__(self):
        if self.root:
            return "<%s (root)>" % type(self).__name__
        return "<%s %r (%r)>" % (type(self).__name__, self.item, self.count)