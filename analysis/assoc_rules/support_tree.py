class SupportTree(object):
    """
    Implementation of a Support Tree used in Dasha algorithm for comparing datasets using association rules.

    This tree stores information about the structured transactions.
    It is assumed that the items in the transactions can be ordered.
    """
    def __init__(self, root_support, sorted_items):
        self._root = SupportNode(None, root_support)
        self._sorted_items = sorted_items

    @property
    def root(self):
        return self._root

    def add_transaction(self, transaction, support=0, increment=True):
        """
        Add the items contained in the transaction to the tree and update their supports.
        @param [] transaction: transaction to add
        @param int support: support of the last node
        @param bool increment: whether to increment support of a found nodes in the path
        """
        sorted_transaction = self.sort_transaction(transaction)
        current_node = self._root
        for item in sorted_transaction:
            next_node = current_node.search(item)
            if next_node:
                #There already is a node in this place. Increase its support.
                if increment:
                    next_node.increment_count()
            else:
                #Node not found, insert it into the tree.
                next_node = SupportNode(item)
                current_node.add_child(next_node)
                if support:
                    next_node.support = support
            current_node = next_node
        if support:
            current_node.support = support

    def sort_transaction(self, transaction):
        """
        Sort items in the transaction following the FP-tree order: first desc by frequency, then asc in natural order.
        @param [] transaction: transaction to sort
        @return sorted transaction
        """
        return sorted(transaction, key=lambda x: self._sorted_items.index(x))

    def get_node_for_pattern(self, pattern, root=None):
        """
        Return last node in path for exact path in tree starting from given node or root.
        For this method, the looked path should exist as is, and only its support is returned.
        I.e. if the pattern is 'ac', only support for 'c' in path 'root->a->c' will be returned.
        Path 'root->a->b->c' won't be considered in this situation.
        Is intended to be used for fast retrieval of paths support from itemsets tree. If using
        with transaction tree, will give wrong values.
        @param [] pattern: pattern to look for
        @param SupportNode or None root: node to start search. If not set, root is used.
        """
        try:
            sorted_pattern = self.sort_transaction(pattern)
        except ValueError:
            #If any of the nodes we look for is not in the tree, we immediately return None
            return None

        if not root:
            current_node = self.root
        else:
            current_node = root

        if not pattern:
            return root

        for child in [c[1] for c in current_node.children]:
            if child.item == sorted_pattern[0]:
                if len(pattern) > 1:
                    return self.get_node_for_pattern(sorted_pattern[1:], child)
                else:
                    return child
        return None

    def inspect(self):
        print 'Tree:'
        self.root.inspect(0)


class SupportNode(object):
    """A node in support tree"""

    def __init__(self, item, support=1):
        self._item = item
        self._support = support
        self._parent = None
        self._children = {}

    def add_child(self, child):
        """
        Add a given node as a child of a current node
        @param SupportNode child: child to add
        """
        if not child.item in self._children:
            self._children[child.item] = child
            child.parent = self

    def search(self, item):
        """If a current node has a child with a given item, return it. Return None otherwise."""
        if item in self._children:
            return self._children[item]
        else:
            return None

    def pattern(self):
        """Return prefix of the tree starting from root down to and including the item"""
        if self.root:
            return None
        elif self.parent.root:
            return [self.item]
        else:
            return self.parent.pattern() + [self.item]

    @property
    def item(self):
        """The item contained in this node"""
        return self._item

    @property
    def support(self):
        """Tree support for the prefix ending at this node"""
        return self._support

    @support.setter
    def support(self, value):
        self._support = value

    def increment(self):
        """Increment node support by 1"""
        if self._support is None:
            raise ValueError('Root nodes do not have support')
        self._support += 1

    @property
    def root(self):
        """True if the node is root, false otherwise"""
        return self._item is None

    @property
    def leaf(self):
        """True if the node is leaf, false otherwise"""
        return len(self._children) == 0

    @property
    def parent(self):
        """Parent node"""
        return self._parent

    @parent.setter
    def parent(self, value):
        if value is not None and not isinstance(value, SupportNode):
            raise TypeError('Only a SupportNode instance can be a parent.')
        self._parent = value

    @property
    def children(self):
        """Return children of this node"""
        return tuple(self._children.iteritems())

    def inspect(self, depth=0):
        if depth > 0:
            print ('|  ' * (depth-1)) + '|--' + repr(self)
        else:
            print repr(self)
        for _, child in sorted(self.children, key=lambda i: i[0]):
            child.inspect(depth + 1)

    def __repr__(self):
        if self.root:
            return "<%s (root)>" % type(self).__name__
        return "%r (%r)" % (self.item, self.support)
