import math
from analysis import FREQUENT_ITEMSETS_GENERATOR
from analysis.assoc_rules.utils import get_item_index
from repo.models import CounterRecordClass


class AssociationRule:
    """
    Is used to store information about the itemsets that are different between the datasets.
    """

    #When calculating rule danger, support is considered less important than severity by this value
    SUPPORT_COEFFICIENT = 5

    def __init__(self, antecedent, baseline_support, target_support, scaling_factor,
                 num_baseline_transactions, num_target_transactions):
        self._antecedent = antecedent
        self._baseline_support = baseline_support
        self._target_support = target_support
        self._scaling_factor = scaling_factor
        self._children = []
        self._num_baseline_transactions = num_baseline_transactions
        self._num_target_transactions = num_target_transactions

    @property
    def num_baseline_transactions(self):
        return self._num_baseline_transactions

    @property
    def num_target_transactions(self):
        return self._num_target_transactions

    @property
    def antecedent(self):
        return self._antecedent

    @property
    def baseline_support(self):
        return self._baseline_support

    @property
    def target_support(self):
        return self._target_support * self._scaling_factor

    @property
    def target_support_unscaled(self):
        return self._target_support

    @property
    def scaling_factor(self):
        return self._scaling_factor

    @property
    def baseline_support_percents(self):
        return int(float(self.baseline_support) / self.num_baseline_transactions * 100)

    @property
    def target_support_percents(self):
        return int(float(self.target_support_unscaled) / self.num_target_transactions * 100)

    @property
    def consequents(self):
        """
        @return [ComparisonDifference] children
        """
        return self._children

    def add_consequent(self, child):
        self.consequents.append(child)
        child.parent = self

    def severity(self, index=0):
        """
        Severity metric is used to find how severe the violation flagged in a rule is.
        Calculates difference between consequent supports in baseline and target to find result.
        @param int index: counter index, if not set then all consequents are considered
        """
        diff = 0
        total = 0
        for c in self.consequents:
            if not index or get_item_index(c.item) == index:
                diff += math.fabs(c.baseline_support - c.target_support)
                total += c.baseline_support + c.target_support
        return float(diff) / total

    def danger(self, index=0):
        """
        Return danger, a cumulative metric consisting of severity and support.
        It is used to select most important rule for given index and sort items for presenting.
        @param int index: counter index, if not set then all consequents are considered
        """
        return self.severity(index) + float(self.baseline_support_percents) / 100 / self.SUPPORT_COEFFICIENT

    def has_consequent_with_index(self, index):
        for c in self.consequents:
            if get_item_index(c.item) == index:
                return True
        return False

    def __str__(self):
        if not self.scaling_factor == 1:
            result = 'Antecedent: %s (supports %.2f, %.2f (%.2f)) \n' \
                     % (self.antecedent, self.baseline_support, self.target_support, self.target_support_unscaled)
        else:
            result = 'Antecedent: %s (supports %d, %d) \n' \
                     % (self.antecedent, self.baseline_support, self.target_support)

        for child in sorted(self.consequents, key=lambda i: i.item):
            result += child.__str__()
        return result

    def str_important(self, minsup):
        """
        Return representation missing non-important consequents.
        """
        result = 'Antecedent: %s (supports %d, %d (%d)) \n' \
                 % (self.antecedent, self.baseline_support, self.target_support, self.target_support_unscaled)

        for child in sorted(self.consequents, key=lambda i: i.item):
            if child.baseline_support >= minsup or child.target_support >= minsup:
                result += child.__str__()
        return result


class ViolatingCounter(AssociationRule):
    def __init__(self, counter_class, antecedent, baseline_support, target_support,
                 num_baseline_transactions, num_target_transactions, min_sup, min_sup_diff, scaling_factor):
        AssociationRule.__init__(self, antecedent, baseline_support, target_support, scaling_factor,
                                 num_baseline_transactions, num_target_transactions)

        self._counter_class = counter_class
        self._min_sup = min_sup
        self._min_sup_diff = min_sup_diff

    @property
    def counter_class(self):
        return self._counter_class

    def get_expanded_prefix(self):
        """Decode the path up to the tree root."""
        result = []
        generator = FREQUENT_ITEMSETS_GENERATOR()
        for item in self.antecedent:
            clazz_id, category = generator.decode_item(item)
            clazz = CounterRecordClass.objects.select_related().get(pk=clazz_id)
            result.append((clazz, category))
        return result

    @property
    def counter_str(self):
        return '%s - %s' % (self.counter_class.counter.name, self.counter_class.label)

    def get_violating_children(self):
        """
        Return the most important violating children.
        @return [Consequent]: children with important violations
        """
        #Step 1: Remove children with baseline and target supports < min_sup
        min_sup_result = [c for c in self.consequents if
                          c.baseline_support >= self._min_sup or c.target_support >= self._min_sup]

        #Step 2: Remove children that have insignificant difference in supports
        result = [c for c in min_sup_result if math.fabs(c.relative_support_difference) >= self._min_sup_diff]

        #If nothing remains after step 2, return results of step 1, otherwise results of step 2
        if not result:
            result = min_sup_result

        return sorted(result, key=lambda x: x.confdiff, reverse=True)

    def get_consequents_for_presentation(self):
        """
        A simpler version of get_violating_children() that returns all children except ones with empty support.
        @return [Consequent]
        """
        result = [c for c in self.consequents if not (c.baseline_support == 0 and c.target_support == 0)]
        return sorted(result, key=lambda x: x.confdiff, reverse=True)

    def danger_css_class(self):
        danger = self.danger()
        #        severity = self.severity()
        #        support = float(self.baseline_support_percents)/100
        #        if severity <= 0.2 and support <= 0.2: return 'alert-success'
        #        if severity <= 0.1 or support <= 0.1: return 'alert-success'
        #        if severity > 0.4 and support > 0.4: return 'alert-error'
        if danger >= 0.5:
            return 'alert-error'
        if danger < 0.3:
            return 'alert-success'
        return ''

    @property
    def categories_distribution(self):
        return [(c.category, c.baseline_support, c.target_support,
                 c.baseline_confidence_percents, c.target_confidence_percents)
                for c in self.get_consequents_for_presentation()]


class Consequent():
    _parent = None
    _category = ''

    def __init__(self, item, baseline_support, target_support):
        """
        @param item: differentiating item
        @param float baseline_support: itemset support in a baseline dataset
        @param float target_support: itemset support in a target dataset
        """
        self._item = item
        self._baseline_support = baseline_support
        self._target_support = target_support

    @property
    def category(self):
        return self._category

    @category.setter
    def category(self, category):
        self._category = category

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, parent):
        self._parent = parent

    @property
    def item(self):
        return self._item

    @property
    def baseline_support(self):
        return self._baseline_support

    @property
    def target_support(self):
        return self._target_support * self.parent.scaling_factor

    @property
    def target_support_unscaled(self):
        return self._target_support

    @property
    def baseline_support_percents(self):
        return float(self.baseline_support) / self.parent.num_baseline_transactions * 100

    @property
    def target_support_percents(self):
        return float(self.target_support_unscaled) / self.parent.num_target_transactions * 100

    @property
    def absolute_support_difference_percents(self):
        return math.fabs(self.baseline_support - self.target_support) / float(self.baseline_support) * 100

    @property
    def relative_support_difference(self):
        return self.baseline_support_percents - self.target_support_percents

    @property
    def baseline_confidence(self):
        parent_support = float(self.parent.baseline_support) if self.parent else 1.0
        if not parent_support:
            return 0
        return self.baseline_support / parent_support

    @property
    def target_confidence(self):
        parent_support = float(self.parent.target_support) if self.parent else 1.0
        if not parent_support:
            return 0
        return self.target_support / parent_support

    @property
    def baseline_confidence_percents(self):
        return self.baseline_confidence * 100

    @property
    def target_confidence_percents(self):
        return self.target_confidence * 100

    @property
    def confdiff(self):
        return math.fabs(self.baseline_confidence_percents - self.target_confidence_percents)

    def __str__(self):
        if not self.parent.scaling_factor == 1:
            return ' consequent %s with supports (%.2f, %.2f(%.2f)) and confidences (%.2f, %.2f)\n' \
                   % (self.item, self.baseline_support, self.target_support, self.target_support_unscaled,
                      self.baseline_confidence, self.target_confidence)
        else:
            return ' consequent %s with supports (%d, %d) and confidences (%.2f, %.2f)\n' \
                   % (self.item, self.baseline_support, self.target_support,
                      self.baseline_confidence, self.target_confidence)
