def get_item_index(item):
    """
    Retrieve item index from its hashed representation (only works for numeric datasets generated from performance repo.
    """
    return item / 10


def get_minimum_support(num_transactions, percentage):
    """
    Convert minimum support in percents into absolute value
    @param int num_transactions: Number of transactions in a datasets
    @param int percentage: minimum support in percents
    @return support: absolute value of minimum support in number of transactions
    """
    return int(num_transactions*percentage/float(100))