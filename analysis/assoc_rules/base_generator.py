"""
An ABC for the real generators
"""
from abc import ABCMeta, abstractmethod
from repo import REPO_CLASSIFIER


class BaseFrequentItemsetsGenerator():
    __metaclass__ = ABCMeta

    classifier = REPO_CLASSIFIER
    multiplier = 10

    @abstractmethod
    def find_frequent_itemsets(self, fp_tree, minsup, max_itemset_length):
        """
        Find frequent itemsets from transactions containing classified performance counters.
        @param FPTree fp_tree: instance of FPTree
        @param int minsup: minimum support (absolute value)
        @param int max_itemset_length: maximal length of an itemset to process
        @return SupportTree: instance of SupportTree containing information about frequent itemsets
        """
        pass

    def hash_transactions(self, transactions):
        """
        Take a list of transactions and convert each item to an integer
        @param [[ConsolidatedCounterRecord]] transactions: list of transactions
        """
        hashed_transactions = []
        for transaction in transactions:
            items = [(item.clazz.id, item.category) for item in transaction]
            new_items = sorted([self.encode_item(item) for item in items])
            hashed_transactions.append(new_items)
        return hashed_transactions

    def encode_item(self, item):
        """
        Convert an item to its hashed representation.
        @param (class_id, category) item: representation of an item
        @return int item: hashed item
        """
        item_category = item[1]
        category_index = 0
        for index, category in enumerate(self.classifier.categories):
            if category == item_category:
                category_index = index
        return item[0] * self.multiplier + category_index

    def decode_item(self, item):
        """
        Convert an item from its hashed representation to tuple.
        @param int item: hashed item
        @return (class_id, category) item: decoded item
        """
        return item / self.multiplier, self.classifier.categories[item % self.multiplier]
