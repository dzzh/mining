from django.utils.translation import ugettext as _
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np

from analysis.assoc_rules.rules_analysis import analyze_data
from repo import REPO_CLASSIFIER


def analysis_barchart(testrun, counter):
    """
    Create a bar chart containing categories distribution for a given counter.
    @param TestRun testrun: Testrun to analyze
    @param int counter: id of interesting CounterRecordClass
    @return FigureCanvas: chart canvas
    """
    rules = analyze_data(testrun)

    violating_counter = None
    for rule in rules:
        if rule.counter_class.id == counter:
            violating_counter = rule

    title = _('Categories distribution for counter %s' % violating_counter.counter_str)

    classifier = REPO_CLASSIFIER(testrun)
    categories = classifier.categories

    data = violating_counter.categories_distribution
    data = sorted(data, key=lambda x: categories.index(x[0]))

    return analysis_barchart_canvas(data, title)


def analysis_barchart_canvas(data, title):
    """
    Create canvas for bar chart with categories distribution.
    @param (str, int, int, int, int) data: tuple (category_name, bsl_sup, tgt_sup, bsl_conf, tgt_conf)
    @param str title: chart title
    @return FigureCanvas canvas
    """
    labels = [x[0] for x in data]
    baseline_support = [x[1] for x in data]
    target_support = [x[2] for x in data]
    baseline_confidence = [x[3] for x in data]
    target_confidence = [x[4] for x in data]
    ind = np.arange(len(data))
    width = 0.35

    fig = Figure(figsize=(9, 5), dpi=80)
    fig.suptitle(title)
    sup = fig.add_subplot(1, 2, 1)
    con = fig.add_subplot(1, 2, 2)
    fig.subplots_adjust(wspace=0.3)

    bsls = sup.bar(ind, baseline_support, width, color='b')
    tgts = sup.bar(ind + width, target_support, width, color='g')

    bslc = con.bar(ind, baseline_confidence, width, color='b')
    tgtc = con.bar(ind + width, target_confidence, width, color='g')

    sup.set_ylabel(_('Support (scaled)'))
    con.set_ylabel(_('Confidence, %'))

    sup.set_xticks(ind + width)
    sup.set_xticklabels(labels)
    sup.legend((bsls[0], tgts[0]), (_('Baseline'), _('Target')), fontsize='small')

    con.set_xticks(ind + width)
    con.set_xticklabels(labels)
    con.legend((bslc[0], tgtc[0]), (_('Baseline'), _('Target')), fontsize='small')

    return FigureCanvas(fig)