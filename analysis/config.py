from livesettings import ConfigurationGroup, config_register, PositiveIntegerValue
from django.utils.translation import ugettext as _

ANALYSIS_GROUP = ConfigurationGroup(
    'analysis',
    _('Settings of the analysis module')
)

config_register(PositiveIntegerValue(
    ANALYSIS_GROUP,
    'MINSUP_PERCENTS',
    description=_('Minsup'),
    help_text=_('Minimum support to consider an itemset frequent (in percents).'),
    ordering=1,
    default=10
))

config_register(PositiveIntegerValue(
    ANALYSIS_GROUP,
    'MINDIFF_PERCENTS',
    description=_('Mindiff'),
    help_text=_('Minimum difference in support to consider violation important (in percents).'),
    ordering=2,
    default=50
))

config_register(PositiveIntegerValue(
    ANALYSIS_GROUP,
    'MAX_ITEMSET_LENGTH',
    description=_('Maximal itemset length to process'),
    help_text=_('The itemsets longer than this length won\'t be generated and processed. 0 to process all itemsets.'),
    ordering=3,
    default=10
))